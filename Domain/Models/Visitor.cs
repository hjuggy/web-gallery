﻿using System;

namespace Domain.Models
{
    public class Visitor : DomainEntity
    {
        public override Guid Id { get; set; }

        public string IP { get; set; }

    }
}
