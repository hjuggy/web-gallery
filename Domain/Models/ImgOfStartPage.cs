﻿using System.Collections.Generic;

namespace Domain.Models
{
    public class ImgOfStartPage<TImage>
    {
        public string Genre { get; set; }
        public IEnumerable<TImage> Images { get; set; }
        public int NumberOfImages { get; set; }
    }
}
