﻿using System;

namespace Domain.Models
{
    public class DisLike : DomainEntity
    {
        public override Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public Guid AuthorId { get; set; }
    }
}