﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public class Role : DomainEntity, IRole<Guid>
    {
        public Role()
        {
           Id = Guid.NewGuid();
        }

        public Role(string name)
            : this()
        {
            Name = name;
        }

        public Role(string name, Guid id)
        {
            Name = name;
            Id = id;
        }

        public override Guid Id { get; set; }
        public string Name { get; set; }
        public List<User> Users{ get; set; }
    }
}