﻿using System;

namespace Domain.Models
{
    public class Comment : DomainEntity
    {
        public override Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public Guid WriterId { get; set; }

        public string WriterName { get; set; }

        public DateTime DateTimeComment { get; set; }

        public string CommentContent { get; set; }
    }
}