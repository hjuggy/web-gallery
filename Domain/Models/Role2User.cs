﻿using System;

namespace Domain.Models
{
    public class Role2User : DomainEntity
    {
        public override Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}