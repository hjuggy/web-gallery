﻿namespace Domain.Models
{
    public class GenreImage
    {
        public string Genre { get; set; }
        public int NumberOfImages { get; set; }
    }
}
