﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public class User : DomainEntity, IUser<Guid>
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(string userName)
        {
            UserName = userName;
        }

        public override Guid Id { get; set; }
        public string UserName { get; set; }
        public bool IsOver18 { get; set; }
        public int ImageTake { get; set; } = 5;
        public string FavoriteGenres { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public List<Role> Roles { get; set; } 
    }
}