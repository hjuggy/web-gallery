﻿namespace Domain.Models
{
    public class GenreSwitcher
    {
        public string Genre { get; set; }

        public bool SwitchOn { get; set; }
    }
}