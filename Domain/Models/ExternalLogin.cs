﻿using System;

namespace Domain.Models
{
    public class ExternalLogin : DomainEntity
    {
        public override Guid Id { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}