﻿using System;

namespace Domain.Models
{
    public abstract class DomainEntity
    {
        public abstract Guid Id { get; set; }
    }
}