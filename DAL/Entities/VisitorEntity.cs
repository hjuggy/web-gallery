﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class VisitorEntity : Entity
    {
        public override Guid Id { get; set; }
        public string IP { get; set; }

        public virtual ICollection<LikeImageEntity> Likes { get; set; }
        public virtual ICollection<DisLikeImageEntity> DisLikes { get; set; }
    }

}