﻿using System;

namespace DAL.Entities
{
    public class ExternalLoginEntity : Entity
    {
        public override Guid Id { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public Guid UserId { get; set; }
        public  UserEntity UserEntity { get; set; }
    }
}
