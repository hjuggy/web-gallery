﻿using System;

namespace DAL.Entities
{
    public class DisLikeImageEntity : Entity
    {
        public override Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public virtual ImageEntity Image { get; set; }

        public Guid AuthorId { get; set; }

        public virtual VisitorEntity Author { get; set; }
    }
}