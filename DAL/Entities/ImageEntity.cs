﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class ImageEntity : Entity
    {
        public override Guid Id { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public string Genre { get; set; }

        public string Description { get; set; }

        public bool IsPrivate { get; set; }

        public DateTime DateTimeLoad { get; set; }

        public Guid AuthorImageId { get; set; }

        public virtual UserEntity AuthorImage { get; set; }

        public virtual ICollection<CommentEntity> Comments { get; set; } 
        public virtual ICollection<LikeImageEntity> Likes { get; set; } 
        public virtual ICollection<DisLikeImageEntity> DisLikes { get; set; } 
    }
}
