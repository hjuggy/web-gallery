﻿using System;

namespace DAL.Entities
{
    public class User2RolesEntity: Entity
    {
        public override Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserEntity User { get; set; }
        public Guid RoleId { get; set; }
        public RoleEntity Role { get; set; }
    }
}
