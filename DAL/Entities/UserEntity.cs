﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class UserEntity : Entity
    {
        public override Guid Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public bool? IsOver18 { get; set; }
        public int? ImageTake { get; set; } = 5;
        public string FavoriteGenres { get; set; }
        public virtual ICollection<ImageEntity> Images { get; set; } = new List<ImageEntity>();
        public virtual ICollection<CommentEntity> Comments { get; set; } = new List<CommentEntity>();
        public virtual ICollection<ExternalLoginEntity> Logins { get; set; } = new List<ExternalLoginEntity>();
        public virtual ICollection<User2RolesEntity> User2Roles { get; set; } = new List<User2RolesEntity>();

        //public virtual ICollection<RoleEntity> Roles { get; set; } = new List<RoleEntity>();
    }
}
