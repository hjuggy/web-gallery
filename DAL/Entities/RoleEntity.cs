﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class RoleEntity : Entity
    {
        public override Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<User2RolesEntity> User2Roles { get; set; } = new List<User2RolesEntity>();
        //public virtual ICollection<UserEntity> Users { get; set; } = new List<UserEntity>();
    }
}