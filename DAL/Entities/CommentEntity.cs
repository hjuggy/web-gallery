﻿using System;

namespace DAL.Entities
{
    public class CommentEntity : Entity
    {
        public override Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public virtual ImageEntity Image { get; set; }

        public Guid WriterId { get; set; }

        public virtual UserEntity Writer { get; set; }

        public DateTime DateTimeComment { get; set; }

        public string CommentContent { get; set; }
    }
}