﻿using System;

namespace DAL.Entities
{
    public abstract class Entity
    {
        public virtual Guid Id { get; set; }
    }
}