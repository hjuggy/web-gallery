﻿using System;
using System.Data.Entity;
using DAL.Configurations;
using DAL.Entities;

namespace DAL
{
    public class WebGalleryDbContext : DbContext, IWebGalleryDbContext
    {
        public WebGalleryDbContext() : base("WebGallery")
        {
            Database.SetInitializer<WebGalleryDbContext>(new CreateDatabaseIfNotExists<WebGalleryDbContext>());
        }

        public DbSet<ImageEntity> Images { get; set; }
        public DbSet<LikeImageEntity> Likes { get; set; }
        public DbSet<DisLikeImageEntity> DisLikes { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<VisitorEntity> Visitors { get; set; }
        public IDbSet<UserEntity> Users { get; set; }
        public IDbSet<RoleEntity> Roles { get; set; }
        public IDbSet<User2RolesEntity> User2Roles { get; set; }
        public IDbSet<ExternalLoginEntity> Logins { get; set; }
        //public IDbSet<ClaimEntity> Claims { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ImageConfiguration());
            modelBuilder.Configurations.Add(new CommentConfiguration());
            modelBuilder.Configurations.Add(new VisitorConfiguration());
            modelBuilder.Configurations.Add(new LikeConfiguration());
            modelBuilder.Configurations.Add(new DisLikeConfiguration());
            modelBuilder.Configurations.Add(new UserEntityConfiguration());
            modelBuilder.Configurations.Add(new RoleEntityConfiguration());
            modelBuilder.Configurations.Add(new ExternalLoginEntityConfiguration());
           // modelBuilder.Configurations.Add(new User2RolesConfiguration());
        }
    }
}
