﻿using System.Data.Entity;
using DAL.Entities;

namespace DAL
{
    public interface IWebGalleryDbContext
    {
        DbSet<ImageEntity> Images { get; set; }
        DbSet<LikeImageEntity> Likes { get; set; }
        DbSet<DisLikeImageEntity> DisLikes { get; set; }
        DbSet<CommentEntity> Comments { get; set; }
        DbSet<VisitorEntity> Visitors { get; set; }
        IDbSet<UserEntity> Users { get; set; }
        IDbSet<RoleEntity> Roles { get; set; }
        IDbSet<User2RolesEntity> User2Roles { get; set; }
        IDbSet<ExternalLoginEntity> Logins { get; set; }
        //IDbSet<ClaimEntity> Claims { get; set; }
    }
}
