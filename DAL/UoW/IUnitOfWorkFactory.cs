﻿namespace DAL.UoW
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }
}
