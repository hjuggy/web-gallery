﻿using System;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        TCommand GetCommand<TCommand>();
        void Commit();
        Task<int> CommitAsync(System.Threading.CancellationToken cancellationToken);
        Task<int> CommitAsync();
    }
}