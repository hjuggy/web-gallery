﻿using Autofac;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private WebGalleryDbContext _context;

        private ILifetimeScope _scope;

        public UnitOfWork(ILifetimeScope scope, IWebGalleryDbContext context)
        {
            _scope = scope;
            _context = (WebGalleryDbContext)context;
        }

        public TCommand GetCommand<TCommand>() => _scope.Resolve<TCommand>(new NamedParameter("context", _context));

        public void Commit() => _context.SaveChanges();

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public async Task<int> CommitAsync(System.Threading.CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public void Dispose() => _context.Dispose();
    }
}
