﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DAL.Entities;
using Domain.Models;

namespace DAL.Queries
{
    public interface IImageQuery: IQuery<ImageEntity,ImageDescript>
    {
        Task<ImageDescript> GetById(Guid id);
        Task<List<ImageDescript>> Find(string search, int take, int skip);
        Task<List<ImageDescript>> GetByGenre(string genre, int take, int skip);
        List<ImgOfStartPage<ImageDescript>> GetImgOfStartPage(int take);
        List<GenreImage> GetGenres();
        Task<int> CountImagesInGenre(string genre);
        Task<int> CountImgFind(string search);
        List<ImageDescript> GetByGenre(string genre, int take);
        Task<int> CountSortImage(Expression<Func<ImageEntity, bool>> predicate);
        Task<List<ImageDescript>> SortImage(Expression<Func<ImageEntity, bool>> predicate, int take, int skip);
        Task<List<ImageDescript>> PopularImage(int take, int skip);
        Task<int> CountImage();
    }
}