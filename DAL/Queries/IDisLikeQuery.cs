﻿using System;
using DAL.Entities;
using Domain.Models;

namespace DAL.Queries
{
    public interface IDisLikeQuery : IQuery<DisLikeImageEntity,DisLike>
    {
        bool IsDisLike(Guid authorId, Guid imageId);
    }
}