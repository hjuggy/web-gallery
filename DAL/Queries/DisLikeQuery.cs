﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;

namespace DAL.Queries
{
    public class DisLikeQuery : Query<DisLikeImageEntity, DisLike>, IDisLikeQuery
    {
        public DisLikeQuery(IWebGalleryDbContext context) : base(context) { }

        public bool IsDisLike(Guid authorId, Guid imageId) => Set.Any(l => l.ImageId == imageId & l.AuthorId == authorId);
    }
}
