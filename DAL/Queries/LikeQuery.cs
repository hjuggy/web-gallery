﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;

namespace DAL.Queries
{
    public class LikeQuery : Query<LikeImageEntity, Like>, ILikeQuery
    {
        public LikeQuery(IWebGalleryDbContext context) : base(context) { }

        public bool IsLike(Guid authorId, Guid imageId) => Set.Any(l => l.ImageId == imageId & l.AuthorId == authorId);
    }
}
