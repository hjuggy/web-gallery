﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public interface IQuery<TEntity, TDomain> where TDomain : DomainEntity
    {
        Task<List<TDomain>> GetAllAsync();
        Task<List<TDomain>> GetWithWhere(Func<TEntity, bool> predicate = null);

        Task<List<TDomain>> PageAllAsync(int skip, int take, CancellationToken cancellationToken = default(CancellationToken));

        Task<TDomain> FindByIdAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken));

        TDomain FindById(Guid id);
    }
}
