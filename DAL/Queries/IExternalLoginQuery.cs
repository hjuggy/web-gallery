﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public interface IExternalLoginQuery : IQuery<ExternalLoginEntity, ExternalLogin>
    {
        Task<List<ExternalLogin>> GetByUserIdAsync(Guid userId);
        Task<ExternalLogin> GetByProviderAndKeyAsync(string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken));
    }
}
