﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public class CommentQuery : Query<CommentEntity, Comment>, ICommentQuery
    {
        public CommentQuery(IWebGalleryDbContext context) : base(context) { }

        public int CountComments(Guid id)
        {
            return Set.Where(x => x.ImageId == id).Count();
            ;
        }

        public List<Comment> GetComments(Guid id, int take, int skip)
        {
            var _comments = Set.Where(x => x.ImageId == id)
                                  .OrderByDescending(x => x.DateTimeComment)
                                  .Skip(skip)
                                  .Take(take)
                                  .ToList();


            return _comments.Map<List<CommentEntity>, List<Comment>>();
        }
    }
}
