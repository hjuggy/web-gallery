﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public interface IRoleQuery : IQuery<RoleEntity,Role>
    {
        Task<Role> FindByNameAsync(string roleName, CancellationToken cancellationToken = default(CancellationToken));
        List<Role> GetRoles(Guid userId);

    }
}
