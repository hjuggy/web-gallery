﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public class ImageQuery : Query<ImageEntity, ImageDescript>, IImageQuery
    {
        public ImageQuery(IWebGalleryDbContext context) : base(context) { }

        public async Task<List<ImageDescript>> GetByGenre(string genre, int take, int skip)
        {
            if (genre == null || genre == "")
            {
                List<ImageEntity> images = await Set.OrderByDescending(x => x.DateTimeLoad)
                                      .Skip(skip)
                                      .Take(take)
                                      .ToListAsync();
                return images.Map<List<ImageEntity>, List<ImageDescript>>();
            }
            else
            {
                var images = await Set.Where(x => x.Genre == genre)
                      .OrderByDescending(x => x.DateTimeLoad)
                      .Skip(skip)
                      .Take(take)
                      .ToListAsync();
                return images.Map<List<ImageEntity>, List<ImageDescript>>();
            }
        }

        public List<ImageDescript> GetByGenre(string genre, int take)
        {
            var images = Set.Where(x => x.Genre == genre)
                             .OrderByDescending(x => x.DateTimeLoad)
                             .Take(take)
                             .ToList();

            return images.Map<List<ImageEntity>, List<ImageDescript>>();
        }

        public async Task<ImageDescript> GetById(Guid id)
        {
            var result = await Set.FindAsync(id);
            return result.Map<ImageEntity, ImageDescript>();
        }

        public List<ImgOfStartPage<ImageDescript>> GetImgOfStartPage(int take)
        {
            var query = (from Image in Set
                         group Image by Image.Genre into ImageGroup
                         select new ImgOfStartPage<ImageEntity>
                         {
                             Genre = ImageGroup.Key,
                             Images = ImageGroup.OrderByDescending(x => x.DateTimeLoad)
                                                .Take(take)
                                                .ToList(),
                             NumberOfImages = ImageGroup.Count(),
                         })
                        .OrderByDescending(x => x.NumberOfImages)
                        .ToList();

            return query.Map<List<ImgOfStartPage<ImageEntity>>, List<ImgOfStartPage<ImageDescript>>>();
        }

        public async Task<int> CountImagesInGenre(string genre)
        {
            if (genre == null || genre == "")
            {
                return await Set.CountAsync();
            }
            else
            {
                return await Set.Where(p => p.Genre == genre).CountAsync();
            }

        }

        public List<GenreImage> GetGenres()
        {
            var query = (from Image in Set
                         group Image by Image.Genre into ImageGroup
                         select new GenreImage
                         {
                             Genre = ImageGroup.Key,
                             NumberOfImages = ImageGroup.Count(),
                         })
                         .OrderByDescending(x => x.NumberOfImages);
            return query.ToList();
        }

        public async Task<List<ImageDescript>> Find(string search, int take, int skip)
        {
            var result = await Set.Where(p => p.Name.Contains(search))
                                  .OrderByDescending(x => x.DateTimeLoad)
                                  .Take(take)
                                  .Skip(skip)
                                  .ToListAsync();

            return result.Map<List<ImageEntity>, List<ImageDescript>>();
        }

        public async Task<int> CountImgFind(string search)
        {
            return await Set.Where(p => p.Name.Contains(search)).CountAsync();
        }

        public async Task<List<ImageDescript>> PopularImage(int take, int skip)
        {
            var result = await Set.OrderByDescending(p=>p.Likes.Count)
                                  .Skip(skip)
                                  .Take(take)
                                  .ToListAsync();

            return result.Map<List<ImageEntity>, List<ImageDescript>>();
        }

        public async Task<int> CountImage()
        {
            return await Set.CountAsync();
        }

        public async Task<List<ImageDescript>> SortImage(Expression<Func<ImageEntity, bool>> predicate, int take, int skip)
        {
            var result = await Set.Where(predicate).OrderByDescending(p => p.DateTimeLoad)
                                  .Skip(skip)
                                  .Take(take)
                                  .ToListAsync();

            return result.Map<List<ImageEntity>, List<ImageDescript>>();
        }

        public async Task<int> CountSortImage(Expression<Func<ImageEntity, bool>> predicate)
        {
            return await Set.Where(predicate).CountAsync();
        }


    }
}