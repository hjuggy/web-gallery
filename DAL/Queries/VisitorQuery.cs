﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;

namespace DAL.Queries
{
    public class VisitorQuery : Query<VisitorEntity, Visitor>, IVisitorQuery
    {
        public VisitorQuery(IWebGalleryDbContext context) : base(context) { }

        public Guid GetIdbyIP(string IP)
        {
            if (Set.Any(x => x.IP.Contains(IP)))
            {
                return Set.First(x => x.IP.Contains(IP)).Id;
            }
            return Guid.Empty;
        }
    }
}
