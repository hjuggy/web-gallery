﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public class RoleQuery : Query<RoleEntity, Role>, IRoleQuery
    {
        public RoleQuery(IWebGalleryDbContext context) : base(context) { }

        public async Task<Role> FindByNameAsync(string roleName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var roleEntity = await Set.FirstOrDefaultAsync(x => x.Name == roleName, cancellationToken).ConfigureAwait(false);
            return roleEntity.Map<RoleEntity, Role>();
        }

        public List<Role> GetRoles(Guid userId)
        {
            return Set.Where(p=>p.User2Roles.Any(t=>t.UserId==userId)).ToList().Map<List<RoleEntity>,List<Role>>();
        }
    }
}
