﻿using System;
using DAL.Entities;
using Domain.Models;

namespace DAL.Queries
{
    public interface IVisitorQuery : IQuery<VisitorEntity,Visitor>
    {
        Guid GetIdbyIP(string IP);
    }
}