﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public interface IUserQuery : IQuery<UserEntity,User>
    {
        Task<User> FindByUserNameAsync(string username, CancellationToken cancellationToken = default(CancellationToken));
        Task<List<string>> GetRoles(Guid Id);
        string GetName(Guid Id);
        List<User> GetUsersByRole(int skip, int take, string roleName);
        Task<int> CountUsersByRole(string roleName);

    }
}
