﻿using System;
using DAL.Entities;
using Domain.Models;

namespace DAL.Queries
{
    public interface ILikeQuery : IQuery<LikeImageEntity,Like> 
    {
        bool IsLike(Guid authorId, Guid imageId);
    }
}