﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public class UserQuery : Query<UserEntity, User>, IUserQuery
    {
        public UserQuery(IWebGalleryDbContext context) : base(context) { }

        public async Task<User> FindByUserNameAsync(string userName, CancellationToken cancellationToken = default(CancellationToken))
        {
            var _user= await Set.FirstOrDefaultAsync(x => x.UserName == userName, cancellationToken).ConfigureAwait(false);
            var result = _user.Map<UserEntity, User>();
            return result;
        }

        public string GetName(Guid Id)
        {
            return  Set.FirstOrDefault(p => p.Id == Id).UserName;
        }

        public async Task<List<string>> GetRoles(Guid userId)
        {
            try
            {
                return await _context.User2Roles.Where(p => p.Id == userId).Select(p=>p.Role.Name).ToListAsync();
            }
            catch (Exception)
            {
                throw new ArgumentNullException("Roles");
            }
        }

        public List<User> GetUsersByRole(int skip, int take, string roleName)
        {
            var role = _context.Roles.FirstOrDefault(p => p.Name == roleName);
            var users = _context.Users.Include(p=>p.User2Roles).Where(t => t.User2Roles.Any(r => r.RoleId == role.Id))
                                       .OrderBy(c => c.UserName)
                                       .Skip(skip).Take(take).ToList();
            var result = users.Map<List<UserEntity>, List<User>>();

            return result;
        }

        public async Task<int> CountUsersByRole(string roleName)
        {
            var role = _context.Roles.FirstOrDefault(p => p.Name == roleName);
            return await Set.Where(p => p.User2Roles.Any(r => r.Id == role.Id)).CountAsync();
        }
    }
}
