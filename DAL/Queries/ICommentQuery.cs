﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public interface ICommentQuery : IQuery<CommentEntity,Comment>
    {
        List<Comment> GetComments(Guid id, int take, int skip);

        int CountComments(Guid id);
    }
}