﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    internal class ExternalLoginQuery : Query<ExternalLoginEntity, ExternalLogin>, IExternalLoginQuery
    {
        public ExternalLoginQuery(IWebGalleryDbContext context)
            : base(context)
        {
        }

        public async Task<ExternalLogin> GetByProviderAndKeyAsync(string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken))
        {
            var result = (await Set.FirstOrDefaultAsync(x => x.LoginProvider == loginProvider && x.ProviderKey == providerKey, cancellationToken).ConfigureAwait(false)).Map<ExternalLoginEntity, ExternalLogin>();

            return result;
        }

        public async Task<List<ExternalLogin>> GetByUserIdAsync(Guid userId)
        {
            var result = (await Set.Where(x => x.UserId == userId).ToListAsync()).Map<List<ExternalLoginEntity>, List<ExternalLogin>>();

            return result;
        }
    }
}
