﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Queries
{
    public class Query<TEntity, TDomain> : IQuery<TEntity, TDomain>
    where TEntity : Entity
    where TDomain : DomainEntity
    {
        internal WebGalleryDbContext _context;
        private DbSet<TEntity> _set;

        public Query(IWebGalleryDbContext context)
        {
            _context = (WebGalleryDbContext)context;
        }

        protected DbSet<TEntity> Set
        {
            get { return _set ?? (_set = _context.Set<TEntity>()); }
        }

        public async Task<List<TDomain>> GetAllAsync() => (await Set.ToListAsync()).Map<List<TEntity>, List<TDomain>>();

        public async Task<List<TDomain>> GetWithWhere(Func<TEntity, bool> predicate = null)
        {
            if (predicate == null)
            {
                return await GetAllAsync();
            }

            return Set.Where(predicate).ToList().Map<List<TEntity>, List<TDomain>>();
        }


        public async Task<List<TDomain>> PageAllAsync(int skip, int take, CancellationToken cancellationToken = default(CancellationToken))
        {
            return (await Set.Skip(skip).Take(take).ToListAsync(cancellationToken)).Map<List<TEntity>, List<TDomain>>();
        }

        public async Task<TDomain> FindByIdAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return (await Set.FindAsync(cancellationToken, id)).Map<TEntity, TDomain>();
        }


        public TDomain FindById(Guid id)
        {
            return  Set.Find(id).Map<TEntity, TDomain>();
        }
    }
}
