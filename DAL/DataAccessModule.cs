﻿using Autofac;
using DAL.Commands;
using DAL.Queries;
using DAL.UoW;

namespace DAL
{
    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //DbContext
            builder.RegisterType<WebGalleryDbContext>().As<IWebGalleryDbContext>();

            //Query
            builder.RegisterType<ImageQuery>().As<IImageQuery>();
            builder.RegisterType<VisitorQuery>().As<IVisitorQuery>();
            builder.RegisterType<CommentQuery>().As<ICommentQuery>();
            builder.RegisterType<LikeQuery>().As<ILikeQuery>();
            builder.RegisterType<DisLikeQuery>().As<IDisLikeQuery>();
            builder.RegisterType<UserQuery>().As<IUserQuery>();
            builder.RegisterType<RoleQuery>().As<IRoleQuery>();
            builder.RegisterType<ExternalLoginQuery>().As<IExternalLoginQuery>();

            //Command    
            builder.RegisterType<ImageCommand>().As<IImageCommand>();
            builder.RegisterType<VisitorCommand>().As<IVisitorCommand>();
            builder.RegisterType<CommentCommand>().As<ICommentCommand>();
            builder.RegisterType<LikeCommand>().As<ILikeCommand>();
            builder.RegisterType<DisLikeCommand>().As<IDisLikeCommand>();
            builder.RegisterType<UserComand>().As<IUserComand>();
            builder.RegisterType<RoleComand>().As<IRoleComand>();
            builder.RegisterType<ExternalLoginComand>().As<IExternalLoginComand>();

            //UoW
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>();
        }
    }
}
