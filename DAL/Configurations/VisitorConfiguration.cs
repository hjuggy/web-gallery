﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class VisitorConfiguration : EntityTypeConfiguration<VisitorEntity>
    {
        public VisitorConfiguration()
        {
            HasKey(p => p.Id);
            ToTable("Visitors");
        }
    }
}
