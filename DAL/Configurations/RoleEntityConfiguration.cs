﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class RoleEntityConfiguration : EntityTypeConfiguration<RoleEntity>
    {
        public RoleEntityConfiguration()
        {
            ToTable("Roles");

            Property(x => x.Name)
                .HasMaxLength(64)
                .IsRequired();
        }
    }
}
