﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class LikeConfiguration : EntityTypeConfiguration<LikeImageEntity>
    {
        public LikeConfiguration()
        {
            //HasRequired(t => t.Image).WithMany(p => p.Likes).HasForeignKey(p => p.ImageId);
            //HasRequired(t => t.Author).WithMany(p => p.Likes).HasForeignKey(p => p.AuthorId);
            ToTable("Likes");
        }
    }
}
