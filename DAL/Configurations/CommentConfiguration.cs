﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class CommentConfiguration : EntityTypeConfiguration<CommentEntity>
    {
        public CommentConfiguration()
        {
            HasKey(p => p.Id);
            //HasRequired(t => t.Image).WithMany(p=>p.Comments).HasForeignKey(p=>p.ImageId);
           // HasRequired(t => t.Writer).WithMany(p => p.Comments).HasForeignKey(p => p.WriterId);
            //Property(p => p.CommentContent).IsRequired().HasMaxLength(300).IsUnicode(true).IsVariableLength().HasParameterName("Comment");
            Property(p => p.CommentContent).IsRequired().HasMaxLength(300).IsVariableLength();
            ToTable("Comments");
        }
    }
}
