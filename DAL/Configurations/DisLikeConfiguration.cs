﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class DisLikeConfiguration : EntityTypeConfiguration<DisLikeImageEntity>
    {
        public DisLikeConfiguration()
        {
            //HasRequired(t => t.Image).WithMany(p => p.DisLikes).HasForeignKey(p => p.ImageId);
            //HasRequired(t => t.Author).WithMany(p => p.DisLikes).HasForeignKey(p => p.AuthorId);
            ToTable("DisLikes");
        }
    }
}
