﻿using DAL.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DAL.Configurations
{
    public class ImageConfiguration : EntityTypeConfiguration<ImageEntity>
    {
        public ImageConfiguration()
        {
            HasKey(p => p.Id);

            //HasRequired(t => t.Author).WithMany(p => p.Images).Map(m =>m.MapKey("AuthorId"));
            Property(p => p.Description).IsOptional().HasMaxLength(600).IsUnicode(true).IsVariableLength();
            Property(p => p.Name).IsOptional().HasMaxLength(20).IsUnicode(true).IsVariableLength();
            Property(p => p.Genre).IsOptional().HasMaxLength(20).IsUnicode(true).IsVariableLength();
            ToTable("Images");
        }
    }
}
