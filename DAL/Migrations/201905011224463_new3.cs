namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleId" });
            CreateTable(
                "dbo.User2RolesEntity",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.RoleEntityUserEntities",
                c => new
                    {
                        RoleEntity_Id = c.Guid(nullable: false),
                        UserEntity_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleEntity_Id, t.UserEntity_Id })
                .ForeignKey("dbo.Roles", t => t.RoleEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserEntity_Id, cascadeDelete: true)
                .Index(t => t.RoleEntity_Id)
                .Index(t => t.UserEntity_Id);
            
            DropColumn("dbo.Users", "RoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "RoleId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.RoleEntityUserEntities", "UserEntity_Id", "dbo.Users");
            DropForeignKey("dbo.RoleEntityUserEntities", "RoleEntity_Id", "dbo.Roles");
            DropForeignKey("dbo.User2RolesEntity", "UserId", "dbo.Users");
            DropForeignKey("dbo.User2RolesEntity", "RoleId", "dbo.Roles");
            DropIndex("dbo.RoleEntityUserEntities", new[] { "UserEntity_Id" });
            DropIndex("dbo.RoleEntityUserEntities", new[] { "RoleEntity_Id" });
            DropIndex("dbo.User2RolesEntity", new[] { "RoleId" });
            DropIndex("dbo.User2RolesEntity", new[] { "UserId" });
            DropTable("dbo.RoleEntityUserEntities");
            DropTable("dbo.User2RolesEntity");
            CreateIndex("dbo.Users", "RoleId");
            AddForeignKey("dbo.Users", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
        }
    }
}
