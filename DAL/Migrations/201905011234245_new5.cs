namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "RoleEntity_Id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleEntity_Id" });
            DropColumn("dbo.Users", "RoleEntity_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "RoleEntity_Id", c => c.Guid());
            CreateIndex("dbo.Users", "RoleEntity_Id");
            AddForeignKey("dbo.Users", "RoleEntity_Id", "dbo.Roles", "Id");
        }
    }
}
