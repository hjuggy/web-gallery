namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RoleEntityUserEntities", "RoleEntity_Id", "dbo.Roles");
            DropForeignKey("dbo.RoleEntityUserEntities", "UserEntity_Id", "dbo.Users");
            DropIndex("dbo.RoleEntityUserEntities", new[] { "RoleEntity_Id" });
            DropIndex("dbo.RoleEntityUserEntities", new[] { "UserEntity_Id" });
            AddColumn("dbo.Users", "RoleEntity_Id", c => c.Guid());
            CreateIndex("dbo.Users", "RoleEntity_Id");
            AddForeignKey("dbo.Users", "RoleEntity_Id", "dbo.Roles", "Id");
            DropTable("dbo.RoleEntityUserEntities");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.RoleEntityUserEntities",
                c => new
                    {
                        RoleEntity_Id = c.Guid(nullable: false),
                        UserEntity_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleEntity_Id, t.UserEntity_Id });
            
            DropForeignKey("dbo.Users", "RoleEntity_Id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleEntity_Id" });
            DropColumn("dbo.Users", "RoleEntity_Id");
            CreateIndex("dbo.RoleEntityUserEntities", "UserEntity_Id");
            CreateIndex("dbo.RoleEntityUserEntities", "RoleEntity_Id");
            AddForeignKey("dbo.RoleEntityUserEntities", "UserEntity_Id", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.RoleEntityUserEntities", "RoleEntity_Id", "dbo.Roles", "Id", cascadeDelete: true);
        }
    }
}
