namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ImageId = c.Guid(nullable: false),
                        WriterId = c.Guid(nullable: false),
                        DateTimeComment = c.DateTime(nullable: false),
                        CommentContent = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.WriterId, cascadeDelete: true)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.WriterId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(maxLength: 20),
                        Extension = c.String(),
                        Genre = c.String(maxLength: 20),
                        Description = c.String(maxLength: 600),
                        IsPrivate = c.Boolean(nullable: false),
                        DateTimeLoad = c.DateTime(nullable: false),
                        AuthorImageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AuthorImageId, cascadeDelete: false)
                .Index(t => t.AuthorImageId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        IsOver18 = c.Boolean(),
                        ImageTake = c.Int(),
                        FavoriteGenres = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ExternalLogins",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User2RolesEntity",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.DisLikes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ImageId = c.Guid(nullable: false),
                        AuthorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Visitors", t => t.AuthorId, cascadeDelete: true)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.Visitors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IP = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ImageId = c.Guid(nullable: false),
                        AuthorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Visitors", t => t.AuthorId, cascadeDelete: true)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.AuthorId);
            
            CreateTable(
                "dbo.User2Roles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DisLikes", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Likes", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Likes", "AuthorId", "dbo.Visitors");
            DropForeignKey("dbo.DisLikes", "AuthorId", "dbo.Visitors");
            DropForeignKey("dbo.Comments", "ImageId", "dbo.Images");
            DropForeignKey("dbo.User2Roles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.User2Roles", "UserId", "dbo.Users");
            DropForeignKey("dbo.User2RolesEntity", "UserId", "dbo.Users");
            DropForeignKey("dbo.User2RolesEntity", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.ExternalLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.Images", "AuthorImageId", "dbo.Users");
            DropForeignKey("dbo.Comments", "WriterId", "dbo.Users");
            DropIndex("dbo.User2Roles", new[] { "RoleId" });
            DropIndex("dbo.User2Roles", new[] { "UserId" });
            DropIndex("dbo.Likes", new[] { "AuthorId" });
            DropIndex("dbo.Likes", new[] { "ImageId" });
            DropIndex("dbo.DisLikes", new[] { "AuthorId" });
            DropIndex("dbo.DisLikes", new[] { "ImageId" });
            DropIndex("dbo.User2RolesEntity", new[] { "RoleId" });
            DropIndex("dbo.User2RolesEntity", new[] { "UserId" });
            DropIndex("dbo.ExternalLogins", new[] { "UserId" });
            DropIndex("dbo.Images", new[] { "AuthorImageId" });
            DropIndex("dbo.Comments", new[] { "WriterId" });
            DropIndex("dbo.Comments", new[] { "ImageId" });
            DropTable("dbo.User2Roles");
            DropTable("dbo.Likes");
            DropTable("dbo.Visitors");
            DropTable("dbo.DisLikes");
            DropTable("dbo.User2RolesEntity");
            DropTable("dbo.Roles");
            DropTable("dbo.ExternalLogins");
            DropTable("dbo.Users");
            DropTable("dbo.Images");
            DropTable("dbo.Comments");
        }
    }
}
