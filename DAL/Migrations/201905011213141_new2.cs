namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class new2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.User2RolesEntity", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.User2RolesEntity", "UserId", "dbo.Users");
            DropForeignKey("dbo.User2Roles", "UserId", "dbo.Users");
            DropForeignKey("dbo.User2Roles", "RoleId", "dbo.Roles");
            DropIndex("dbo.User2RolesEntity", new[] { "UserId" });
            DropIndex("dbo.User2RolesEntity", new[] { "RoleId" });
            DropIndex("dbo.User2Roles", new[] { "UserId" });
            DropIndex("dbo.User2Roles", new[] { "RoleId" });
            AddColumn("dbo.Users", "RoleId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Users", "RoleId");
            AddForeignKey("dbo.Users", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
            DropTable("dbo.User2RolesEntity");
            DropTable("dbo.User2Roles");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.User2Roles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId });
            
            CreateTable(
                "dbo.User2RolesEntity",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Users", "RoleId", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "RoleId" });
            DropColumn("dbo.Users", "RoleId");
            CreateIndex("dbo.User2Roles", "RoleId");
            CreateIndex("dbo.User2Roles", "UserId");
            CreateIndex("dbo.User2RolesEntity", "RoleId");
            CreateIndex("dbo.User2RolesEntity", "UserId");
            AddForeignKey("dbo.User2Roles", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.User2Roles", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.User2RolesEntity", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.User2RolesEntity", "RoleId", "dbo.Roles", "Id", cascadeDelete: true);
        }
    }
}
