﻿using System;
using Domain.Models;

namespace DAL.Commands
{
    public interface IDisLikeCommand : IComand<DisLike>
    {
        void Remove(Guid authorId, Guid imageId);
        void RemoveAll(Guid imageId);
    }
}