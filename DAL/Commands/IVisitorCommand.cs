﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace DAL.Commands
{
    public interface IVisitorCommand : IComand<Visitor>
    {
        Task Remove(Guid IdVisitor);
    }
}