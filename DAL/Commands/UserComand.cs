﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;

namespace DAL.Commands
{
    public class UserComand : Comand<UserEntity, User>, IUserComand
    {
       public UserComand(WebGalleryDbContext context) : base(context) { }
    }
}
