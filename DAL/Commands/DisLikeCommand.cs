﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;

namespace DAL.Commands
{
    public class DisLikeCommand : Comand<DisLikeImageEntity, DisLike>, IDisLikeCommand
    {
        public DisLikeCommand(WebGalleryDbContext context) : base(context) { }

        public void Remove(Guid authorId, Guid imageId)
        {
            var Dislike = Set.FirstOrDefault(l => l.ImageId == imageId & l.AuthorId == authorId);
            if (Dislike!=null)
            {
                Set.Remove(Dislike);
                _context.SaveChanges();
            }
        }

        public void RemoveAll(Guid imageId)
        {
            var Dislikes = Set.Where(l => l.ImageId == imageId);
            if (Dislikes != null)
            {
                Set.RemoveRange(Dislikes);
                _context.SaveChanges();
            }
        }
    }
}
