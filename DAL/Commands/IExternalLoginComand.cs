﻿using Domain.Models;

namespace DAL.Commands
{
    public interface IExternalLoginComand: IComand<ExternalLogin>
    {
    }
}
