﻿using DAL.Entities;
using Domain.Models;

namespace DAL.Commands
{
    internal class ExternalLoginComand : Comand<ExternalLoginEntity, ExternalLogin>, IExternalLoginComand
    {
        public ExternalLoginComand(WebGalleryDbContext context)
            : base (context)
        {
        }
    }
}
