﻿using Domain.Models;

namespace DAL.Commands
{
    public interface IComand<TDomain> where TDomain : DomainEntity
    {
        void Add(TDomain entity);
        void Update(TDomain entity);
        void Remove(TDomain entity);
    }
}
