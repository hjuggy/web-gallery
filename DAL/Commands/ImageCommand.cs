﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public class ImageCommand : Comand<ImageEntity, ImageDescript>, IImageCommand
    {
        public ImageCommand(WebGalleryDbContext context) : base(context) { }

        public async Task Remove(Guid IdImage)
        {
            var image = await Set.FindAsync(IdImage);
            Set.Remove(image);
            _context.SaveChanges();
        }

        public async Task AddComment(Comment comment)
        {
            var image = await Set.FindAsync(comment.ImageId);
            image.Comments.Add(comment.Map<Comment, CommentEntity>());
            await _context.SaveChangesAsync();
        }

        public async Task SaveDescript(Guid id, string description)
        {
            var image = await Set.FindAsync(id);
            image.Description = description;            
            await _context.SaveChangesAsync();
        }
    }
}
