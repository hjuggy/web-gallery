﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public class LikeCommand : Comand<LikeImageEntity, Like>, ILikeCommand
    {
        public LikeCommand(WebGalleryDbContext context) : base(context) { }

        public void Remove(Guid authorId, Guid imageId)
        {
            var like = Set.FirstOrDefault(l => l.ImageId == imageId & l.AuthorId == authorId);
            if (like != null)
            {
                Set.Remove(like);
                _context.SaveChanges();
            }
        }

        public void RemoveAll(Guid imageId)
        {
            var likes = Set.Where(l => l.ImageId == imageId);
            if (likes != null)
            {
                Set.RemoveRange(likes);
                _context.SaveChanges();
            }
        }
    }
}
