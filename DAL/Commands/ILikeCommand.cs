﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace DAL.Commands
{
    public interface ILikeCommand : IComand<Like>
    {
        void Remove(Guid authorId, Guid imageId);
        void RemoveAll(Guid imageId);
    }
}