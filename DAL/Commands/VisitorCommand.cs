﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public class VisitorCommand : Comand<VisitorEntity, Visitor>, IVisitorCommand
    {
        public VisitorCommand(WebGalleryDbContext context) : base(context) { }

        public async Task Remove(Guid IdVisitor)
        {
            var visitor = await Set.FindAsync(IdVisitor);
            if (visitor!=null)
            {
                Set.Remove(visitor);
                _context.SaveChanges();
            }
        }
    }
}
