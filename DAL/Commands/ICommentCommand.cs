﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace DAL.Commands
{
    public interface ICommentCommand : IComand<Comment>
    {
        Task Remove(Guid IdComment);
        Task RemoveAll(Guid imageId);
    }
}