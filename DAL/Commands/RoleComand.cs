﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public class RoleComand : Comand<RoleEntity, Role>, IRoleComand
    {
        public RoleComand(WebGalleryDbContext context) : base(context) { }
        public async Task Update(Guid userId, string roleName)
        {

            var role = await Set.FirstOrDefaultAsync(p => p.Name == roleName);
            if (role==null)
            {
                role = new RoleEntity()
                {
                    Id = Guid.NewGuid(),
                    Name = roleName,
                };
                Set.Add(role);
            }

            var role2user = new User2RolesEntity { Id = Guid.NewGuid(), RoleId = role.Id, UserId = userId, };
            _context.User2Roles.Add(role2user);
            _context.SaveChanges();
        }

        public async Task Remove(Guid userId, string roleName)
        {
            var role = await Set.FirstOrDefaultAsync(p => p.Name == roleName);
            if (role != null)
            {
                var delEntity = await _context.User2Roles.FirstOrDefaultAsync(p => p.UserId == userId && p.RoleId == role.Id);
                _context.User2Roles.Remove(delEntity);
                _context.SaveChanges();
            }
        }
    }
}
