﻿using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using System.Data.Entity;

namespace DAL.Commands
{
    public class Comand<TEntity, TDomain> : IComand<TDomain>
        where TEntity : Entity
        where TDomain : DomainEntity
    {
        internal WebGalleryDbContext _context;
        private DbSet<TEntity> _set;

        public Comand(WebGalleryDbContext context)
        {
            _context = context;
        }

        protected DbSet<TEntity> Set
        {
            get { return _set ?? (_set = _context.Set<TEntity>()); }
        }

        public void Add(TDomain entity)
        {
            Set.Add(entity.Map<TDomain, TEntity>());
            _context.SaveChanges();
        }

        public void Update(TDomain entity)
        {
            var _entiry = Set.Find(entity.Id);
            entity.Map(_entiry);
            //_entiry.Map(entity);
            _context.SaveChanges();
        }

        public void Remove(TDomain entity)
        {
            Set.Remove(entity.Map<TDomain, TEntity>());
            _context.SaveChanges();
        }
    }
}
