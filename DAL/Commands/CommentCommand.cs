﻿using DAL.Entities;
using Domain.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public class CommentCommand : Comand<CommentEntity, Comment>, ICommentCommand
    {
        public CommentCommand(WebGalleryDbContext context) : base(context) { }

        public async Task Remove(Guid IdComment)
        {
            var comment = await Set.FindAsync(IdComment);
            if (comment!=null)
            {
                Set.Remove(comment);
                await _context.SaveChangesAsync();
            }
        }

        public async Task RemoveAll(Guid imageId)
        {
            var comments = Set.Where(p => p.ImageId == imageId);
            if (comments!=null)
            {
                Set.RemoveRange(comments);
                await _context.SaveChangesAsync();
            }
        }
    }
}
