﻿using Domain.Models;
using System;
using System.Threading.Tasks;

namespace DAL.Commands
{
    public interface IRoleComand : IComand<Role>
    {
        Task Update(Guid userId, string roleName);
        Task Remove(Guid userId, string roleName);
    }
}
