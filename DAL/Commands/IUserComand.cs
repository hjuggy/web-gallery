﻿using Domain.Models;
using System;

namespace DAL.Commands
{
    public interface IUserComand : IComand<User>
    {
    }
}
