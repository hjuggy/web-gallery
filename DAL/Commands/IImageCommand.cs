﻿using System;
using System.Threading.Tasks;
using Domain.Models;

namespace DAL.Commands
{
    public interface IImageCommand : IComand<ImageDescript>
    {
        Task Remove(Guid IdImage);
        Task AddComment(Comment comment);
        Task SaveDescript(Guid id, string description);
    }
}