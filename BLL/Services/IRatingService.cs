﻿using System;
using System.Threading.Tasks;

namespace BLL.Services
{
    public interface IRatingService
    {
        Task<int> CountDisLikes(Guid imageId);
        Task<int> CountLikes(Guid imageId);
        void TouchDisLike(string IP, Guid imageId);
        void TouchLike(string IP, Guid imageId);
    }
}