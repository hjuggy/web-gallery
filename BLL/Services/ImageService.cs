﻿using Autofac;
using DAL.Commands;
using DAL.Entities;
using DAL.Queries;
using DAL.UoW;
using Domain;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ImageService : IImageService
    {

        private IUnitOfWorkFactory _factory;
        private IImageQuery _imageQuery;
        private IUserQuery _userQuery;
        private ILikeQuery _likeQuery;
        private IDisLikeQuery _dislikeQuery;


        public ImageService(IUnitOfWorkFactory factory, IImageQuery imageQuery, ILikeQuery likeQuery, IDisLikeQuery dislikeQuery, IUserQuery userQuery)
        {
            _factory = factory;
            _imageQuery = imageQuery;
            _likeQuery = likeQuery;
            _dislikeQuery = dislikeQuery;
            _userQuery = userQuery;
        }

        public void Add(ImageDescript image)
        {
            using (var UnitOfWork = _factory.Create())
            {
                image.Extension = Path.GetExtension(image.Path);
                UnitOfWork.GetCommand<IImageCommand>().Add(image);
                UnitOfWork.Commit();
            }
        }

        public async Task<int> CountImagesInGenre(string genre) => await _imageQuery.CountImagesInGenre(genre);

        public async Task<IEnumerable<ImageDescript>> GetByGenre(string genre, int take, int skip)
        {
            var images = await _imageQuery.GetByGenre(genre, take, skip);

            foreach (var image in images)
            {
                image.Path = GetPath(image.Id.ToString(), image.Extension);
                await CountRating(image);
            }

            return images;
        }

        public async Task<ImageDescript> GetById(Guid id)
        {
            var image = await _imageQuery.GetById(id);
            image.Path = GetPath(image.Id.ToString(), image.Extension);
            await CountRating(image);
            return image;
        }

        public IEnumerable<GenreImage> GetGenres()
        {
            var genres = _imageQuery.GetGenres();

            return genres;
        }

        public IEnumerable<GenreImage> GetGenres(Guid idUser)
        {
            var genres = _imageQuery.GetGenres();

            if (idUser != Guid.Empty)
            {
                string favoriteGenres = _userQuery.FindById(idUser).FavoriteGenres;
                if (favoriteGenres != null)
                {
                    var favoriteGenresList = GenresString2List(favoriteGenres);
                    var favoriteGenresListTrue = favoriteGenresList.Where(p => p.SwitchOn == true);

                    var result = genres.Where(p => favoriteGenresListTrue.Any(f => f.Genre == p.Genre));
                    return result;
                }
            }

            return genres;
        }

        public  List<ImgOfStartPage<ImageDescript>> GetImgOfStartPage(Guid idUser)
        {
            int take = 5;
            if (idUser != Guid.Empty)
            {
                var user = _userQuery.FindById(idUser);
                take = user.ImageTake;
                var favoriteGenresList = GetGenres(idUser);
                var result = new List<ImgOfStartPage<ImageDescript>>();
                foreach (var favoriteGenre in favoriteGenresList)
                {
                    result.Add(new ImgOfStartPage<ImageDescript>
                    {
                        Genre = favoriteGenre.Genre,
                        NumberOfImages = favoriteGenre.NumberOfImages,
                        Images = _imageQuery.GetByGenre(favoriteGenre.Genre, take),
                    });
                }

                foreach (var coll in result)
                {
                    foreach (var image in coll.Images)
                    {
                        image.Path = GetPath(image.Id.ToString(), image.Extension);
                        CountRating(image);
                    }
                }
                return result;
            }
            else
            {
                var result = _imageQuery.GetImgOfStartPage(take);

                foreach (var coll in result)
                {
                    foreach (var image in coll.Images)
                    {
                        image.Path = GetPath(image.Id.ToString(), image.Extension);
                        CountRating(image);
                    }
                }
                return result;
            }
        }

        public async Task<IEnumerable<ImageDescript>> Find(string search, int take, int skip = 0)
        {
            IEnumerable<ImageDescript> images = await _imageQuery.Find(search, take, skip);

            foreach (var image in images)
            {
                image.Path = GetPath(image.Id.ToString(), image.Extension);
                await CountRating(image);
            }

            return images;
        }

        public async Task<int> CountImgFind(string search)
        {
            return await _imageQuery.CountImgFind(search);
        }

        public async Task SaveDescript(Guid id, string description)
        {
            using (var UnitOfWork = _factory.Create())
            {
                 await UnitOfWork.GetCommand<IImageCommand>().SaveDescript( id, description);
                UnitOfWork.Commit();
            }
        }



        public async Task Remove(Guid imageId)
        {
            using (var UnitOfWork = _factory.Create())
            {
                await UnitOfWork.GetCommand<ICommentCommand>().RemoveAll(imageId);
                UnitOfWork.GetCommand<ILikeCommand>().RemoveAll(imageId);
                UnitOfWork.GetCommand<IDisLikeCommand>().RemoveAll(imageId);
                await UnitOfWork.GetCommand<IImageCommand>().Remove(imageId);
                UnitOfWork.Commit();
            }
        }


        public Task<int> CountSortImg()
        {
            throw new NotImplementedException();
        }

        public async Task<int> CountSortImage(Expression<Func<ImageEntity, bool>> predicate)
        {
           return await _imageQuery.CountSortImage(predicate);
        }

        public async Task<List<ImageDescript>> SortImage(Expression<Func<ImageEntity, bool>> predicate, int take, int skip)
        {
            var images= await _imageQuery.SortImage(predicate, take, skip);

            foreach (var image in images)
            {
                image.Path = GetPath(image.Id.ToString(), image.Extension);
                await CountRating(image);
            }
            return images;
        }


        public async Task<int> CountImage()
        {
            return await _imageQuery.CountImage();
        }

        public async Task<List<ImageDescript>> PopularImage(int take, int skip)
        {
            var images = await _imageQuery.PopularImage(take, skip);

            foreach (var image in images)
            {
                image.Path = GetPath(image.Id.ToString(), image.Extension);
                await CountRating(image);
            }
            return images;
        }

        private string GetPath(string name, string extension)
        {
            StringBuilder builder = new StringBuilder(@"~/Files/");
            builder.AppendFormat("{0}", name);
            return builder.AppendFormat("{0}", extension).ToString();
        }

        private async Task CountRating(ImageDescript image)
        {
            if (image != null)
            {
                image.Like = (await _likeQuery.GetWithWhere(l => l.ImageId == image.Id)).Count();
                image.DisLike = (await _dislikeQuery.GetWithWhere(l => l.ImageId == image.Id)).Count();
            }
        }

        private List<GenreSwitcher> GenresString2List(string favoriteGenres)
        {
            var genresList = GetGenres();
            var listGenreSwitcher = genresList.Select(p => new GenreSwitcher { Genre = p.Genre, SwitchOn = false }).ToList();
            if (favoriteGenres != null)
            {
                string[] seporator = { "###" };
                var favoriteGenresArr = favoriteGenres.Split(seporator, StringSplitOptions.RemoveEmptyEntries).ToList();

                foreach (var item in listGenreSwitcher)
                {
                    if (favoriteGenresArr.Contains(item.Genre))
                    {
                        item.SwitchOn = true;
                    }
                }
            }

            return listGenreSwitcher.ToList();
        }

        private string GenresList2String(List<GenreSwitcher> favoriteGenresList)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in favoriteGenresList)
            {
                if (item.SwitchOn)
                {
                    builder.AppendFormat("{0}{1}", item.Genre, "###");
                }

            }

            return builder.ToString();
        }
    }
}
