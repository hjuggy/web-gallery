﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Entities;
using Domain.Models;

namespace BLL.Services
{
    public interface IImageService
    {
        void Add(ImageDescript image);
        Task<IEnumerable<ImageDescript>> GetByGenre(string genre, int take, int skip);
        List<ImgOfStartPage<ImageDescript>> GetImgOfStartPage(Guid IdUser);
        IEnumerable<GenreImage> GetGenres(Guid idUser);
        IEnumerable<GenreImage> GetGenres();
        Task<int> CountImagesInGenre(string genre);
        Task<ImageDescript> GetById(Guid id);
        Task<IEnumerable<ImageDescript>> Find(string search, int take, int skip = 0);

        Task Remove(Guid imageId);

        Task<int> CountImgFind(string search);
        Task SaveDescript(Guid id, string description);
        Task<int> CountSortImg();

        Task<int> CountSortImage(System.Linq.Expressions.Expression<Func<ImageEntity, bool>> predicate);
        Task<List<ImageDescript>> SortImage(System.Linq.Expressions.Expression<Func<ImageEntity, bool>> predicate, int take, int skip);

        Task<List<ImageDescript>> PopularImage(int take, int skip);
        Task<int> CountImage();
    }
}