﻿using DAL.Commands;
using DAL.Queries;
using DAL.UoW;
using Domain.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class RatingService : IRatingService
    {
        private IUnitOfWorkFactory _factory;
        private IVisitorQuery _visitorQuery;
        private IVisitorService _visitorService;
        private ILikeQuery _likeQuery;
        private IDisLikeQuery _dislikeQuery;


        public RatingService(IUnitOfWorkFactory factory,
            IVisitorQuery visitorQuery,
            ILikeQuery likeQuery,
            IDisLikeQuery dislikeQuery,
            IVisitorService visitorService)
        {
            _factory = factory;
            _visitorQuery = visitorQuery;
            _visitorService = visitorService;
            _likeQuery = likeQuery;
            _dislikeQuery = dislikeQuery;
        }

        public void TouchLike(string IP, Guid imageId)
        {
            Guid authorId = _visitorQuery.GetIdbyIP(IP);
            if (authorId == Guid.Empty)
            {
                _visitorService.Add(IP);
            }

            if (!_likeQuery.IsLike(authorId, imageId))
            {
                using (var UnitOfWork = _factory.Create())
                {
                    var like = new Like
                    {
                        AuthorId = authorId,
                        Id = Guid.NewGuid(),
                        ImageId = imageId,
                    };

                    UnitOfWork.GetCommand<ILikeCommand>().Add(like);

                    if (_dislikeQuery.IsDisLike(authorId, imageId))
                    {
                        UnitOfWork.GetCommand<IDisLikeCommand>().Remove(authorId, imageId);
                    }
                    UnitOfWork.Commit();
                }
            }
            else
            {
                using (var UnitOfWork = _factory.Create())
                {
                    UnitOfWork.GetCommand<ILikeCommand>().Remove(authorId, imageId);
                    UnitOfWork.Commit();
                }
            }
        }

        public void TouchDisLike(string IP, Guid imageId)
        {
            Guid authorId = _visitorQuery.GetIdbyIP(IP);
            if (authorId == Guid.Empty)
            {
                _visitorService.Add(IP);
            }

            if (!_dislikeQuery.IsDisLike(authorId, imageId))
            {
                using (var UnitOfWork = _factory.Create())
                {
                    var Dislike = new DisLike
                    {
                        AuthorId = authorId,
                        Id = Guid.NewGuid(),
                        ImageId = imageId,
                    };

                    UnitOfWork.GetCommand<IDisLikeCommand>().Add(Dislike);

                    if (_likeQuery.IsLike(authorId, imageId))
                    {
                        UnitOfWork.GetCommand<ILikeCommand>().Remove(authorId, imageId);
                    }
                    UnitOfWork.Commit();
                }
            }
            else
            {
                using (var UnitOfWork = _factory.Create())
                {
                    UnitOfWork.GetCommand<IDisLikeCommand>().Remove(authorId, imageId);
                    UnitOfWork.Commit();
                }
            }
        }

        public async Task<int> CountLikes(Guid imageId)
        {
            return (await _likeQuery.GetWithWhere(l => l.ImageId == imageId)).Count();
        }

        public async Task<int> CountDisLikes(Guid imageId)
        {
            return (await _dislikeQuery.GetWithWhere(l => l.ImageId == imageId)).Count();
        }
    }
}
