﻿using DAL.Commands;
using DAL.Queries;
using DAL.UoW;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CommentService : ICommentService
    {
        private IUnitOfWorkFactory _factory;
        private ICommentQuery _comentQuery;
        private IUserQuery _userQuery;

        public CommentService(IUnitOfWorkFactory factory, ICommentQuery comentQuery, IUserQuery userQuery)
        {
            _factory = factory;
            _comentQuery = comentQuery;
            _userQuery = userQuery;
        }

        public void Add(Comment comment)
        {
            using (var unitOfWork= _factory.Create())
            {
                // 1 варинт через CommentCommand
                unitOfWork.GetCommand<ICommentCommand>().Add(comment);

                // 2 варинт через ImageCommand
                //await unitOfWork.GetCommand<IImageCommand>().AddComment(comment);

                //unitOfWork.Commit();
            }
        }
        public async Task Remove( Guid id)
        {
            using (var unitOfWork = _factory.Create())
            {
                await unitOfWork.GetCommand<ICommentCommand>().Remove(id);
                await unitOfWork.CommitAsync();
            }
        }
        
        public  List<Comment> GetComments(Guid id, int take, int skip)
        {
            return  _comentQuery.GetComments(id, take, skip);
        }

        public int CountComments(Guid id)
        {
            return  _comentQuery.CountComments(id);
        }
    }
}
