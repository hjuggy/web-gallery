﻿using Autofac;
using DAL.Commands;
using DAL.Entities;
using DAL.Queries;
using DAL.UoW;
using Domain.Models;
using System;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class VisitorService : IVisitorService
    {
        private IUnitOfWorkFactory _factory;
        private IVisitorQuery _visitorQuery;


        public VisitorService(IUnitOfWorkFactory factory, IVisitorQuery visitorQuery)
        {
            _factory = factory;
            _visitorQuery = visitorQuery;
        }

        public void Add(string IP)
        {
            var temp = _visitorQuery.GetIdbyIP(IP);
            if (temp == Guid.Empty)
            {
                using (var UnitOfWork = _factory.Create())
                {
                    Visitor visitor = new Visitor();
                    visitor.Id = Guid.NewGuid();
                    visitor.IP = IP;
                    UnitOfWork.GetCommand<IVisitorCommand>().Add(visitor);
                    UnitOfWork.Commit();
                }
            }
        }
    }
}
