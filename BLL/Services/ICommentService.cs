﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;

namespace BLL.Services
{
    public interface ICommentService
    {
        void Add(Comment comment);
        List<Comment> GetComments(Guid id, int take, int skip);
        int CountComments(Guid id);
        Task Remove(Guid id);
    }
}