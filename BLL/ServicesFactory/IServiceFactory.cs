﻿namespace BLL.ServicesFactory
{
    public interface IServiceFactory
    {
        T Create<T>();
    }
}