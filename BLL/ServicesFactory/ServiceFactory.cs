﻿using Autofac;
using System.Web.Mvc;

namespace BLL.ServicesFactory
{
    public class ServiceFactory : IServiceFactory
    {
        public ServiceFactory()
        {
        }

        public T Create<T>() => DependencyResolver.Current.GetService<T>();
    }
}
