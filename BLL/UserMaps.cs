﻿using Autofac;
using BLL.Services;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserMap : IUserMap
    {
        private IImageService _imageService;

        public UserMap(IImageService imageService)
        {
            _imageService = imageService;
        } 

        public List<GenreSwitcher> GenresString2List(string favoriteGenres)
        {
            var genresList = _imageService.GetGenres();
            var listGenreSwitcher = genresList.Select(p => new GenreSwitcher { Genre = p.Genre, SwitchOn = false }).ToList();
            if (favoriteGenres!=null)
            {
                string[] seporator = { "###" };
                var favoriteGenresArr = favoriteGenres.Split(seporator, StringSplitOptions.RemoveEmptyEntries).ToList();

                foreach (var item in listGenreSwitcher)
                {
                    if (favoriteGenresArr.Contains(item.Genre))
                    {
                        item.SwitchOn = true;
                    }
                }
            }

            return listGenreSwitcher;
        }

        public string GenresList2String(List<GenreSwitcher> favoriteGenresList)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in favoriteGenresList)
            {
                if (item.SwitchOn)
                {
                    builder.AppendFormat("{0}{1}", item.Genre, "###");
                }

            }

            return builder.ToString();
        }
    }
}
