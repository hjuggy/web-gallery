﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL.Commands;
using DAL.Queries;
using DAL.UoW;
using Domain.Models;
using Microsoft.AspNet.Identity;
namespace BLL.IdentityService
{
    public class RoleStore : IRoleStore<Role, Guid>, IQueryableRoleStore<Role, Guid>
    {
        private IUnitOfWorkFactory _factory;
        private IRoleQuery _roleQuery;

        public RoleStore(IUnitOfWorkFactory factory, IRoleQuery roleQuery)
        {
            _factory = factory;
            _roleQuery = roleQuery;
        }

        public async Task CreateAsync(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IRoleComand>().Add(role);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task DeleteAsync(Role role)
        {
            if (role == null)
                throw new ArgumentNullException("role");

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IRoleComand>().Remove(role);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task<Role> FindByIdAsync(Guid roleId) => await _roleQuery.FindByIdAsync(roleId);

        public async Task<Role> FindByNameAsync(string roleName) => await _roleQuery.FindByNameAsync(roleName);

        public async Task UpdateAsync(Role role)
        {
            if (role == null)
                throw new ArgumentNullException("role");

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IRoleComand>().Update(role);
                await _unitOfWork.CommitAsync();
            }
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public IQueryable<Role> Roles
        {
            get
            {
                return  _roleQuery.GetAllAsync().Result.Select(x => x).AsQueryable();
            }
        }

    }

}
