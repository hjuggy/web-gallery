﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.UoW;
using Domain.Models;
using Microsoft.AspNet.Identity;
using DAL.Queries;
using DAL.Commands;

namespace BLL.IdentityService
{
    public class UserStore : IUserStore
    {

        private IUnitOfWorkFactory _factory;
        private IUserQuery _userQuery;
        private IRoleQuery _roleQuery;
        private IExternalLoginQuery _loginQuery;

        public UserStore(IUnitOfWorkFactory factory, IUserQuery userQuery, IExternalLoginQuery loginQuery, IRoleQuery roleQuery)
        {
            _factory = factory;
            _userQuery = userQuery;
            _loginQuery = loginQuery;
            _roleQuery = roleQuery;
        }

        public List<Role> GetRolesbyId(Guid userId)
        {
            return _roleQuery.GetRoles(userId);
        }

        public List<User> GetUsersByRole(int skip, int take, string role)
        {
            var result = _userQuery.GetUsersByRole(skip, take, role);
            foreach (var item in result)
            {
                item.Roles = _roleQuery.GetRoles(item.Id);
            }
            return result;
        }

        public async Task<int> CountUsersByRole(string role)
        {
            return await _userQuery.CountUsersByRole(role);
        }

        public async Task CreateAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IUserComand>().Add(user);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task DeleteAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IUserComand>().Remove(user);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task<User> FindByIdAsync(Guid userId)
        {
            var result = await _userQuery.FindByIdAsync(userId);
            result.Roles = _roleQuery.GetRoles(userId);
            return result;
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            var user = await _userQuery.FindByUserNameAsync(userName);
            //user.Roles=_roleQuery.GetRoles(user.Id);
            return user;
        }

        public async Task UpdateAsync(User user)
        {
            if (user == null)
                throw new ArgumentException("user");
            if (_userQuery.FindByIdAsync(user.Id) == null)
                throw new ArgumentException("User does not correspond to a User entity.", "user");

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IUserComand>().Update(user);
                await _unitOfWork.CommitAsync();
            }
        }

        public void Dispose()
        {
            // Dispose does nothing since we want Unity to manage the lifecycle of our Unit of Work
        }

        public async Task AddLoginAsync(User user, UserLoginInfo login)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (login == null)
                throw new ArgumentNullException("login");

            if (_userQuery.FindByIdAsync(user.Id) == null)
                throw new ArgumentException("User does not correspond to a User entity.", "user");

            ExternalLogin _login = new ExternalLogin
            {
                Id = Guid.NewGuid(),
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = user.Id,
                User = user,
            };

            using (var _unitOfWork = _factory.Create())
            {
                _unitOfWork.GetCommand<IExternalLoginComand>().Add(_login);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task<User> FindAsync(UserLoginInfo login)
        {
            if (login == null)
                throw new ArgumentNullException("login");

            var _login = await _loginQuery.GetByProviderAndKeyAsync(login.LoginProvider, login.ProviderKey);
            if (_login != null)
            {
                _login.User.Roles = _roleQuery.GetRoles(_login.User.Id);
                return _login.User;
            }
            throw new ArgumentNullException("user");
        }

        public async Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            try
            {
                return (await _loginQuery.GetByUserIdAsync(user.Id)).Select(x => new UserLoginInfo(x.LoginProvider, x.ProviderKey)).ToList();

            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("login");
            }
        }

        public async Task RemoveLoginAsync(User user, UserLoginInfo login)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (login == null)
                throw new ArgumentNullException("login");

            var _login = await _loginQuery.GetByProviderAndKeyAsync(login.LoginProvider, login.ProviderKey);
            if (_login != null)
            {
                using (var _unitOfWork = _factory.Create())
                {
                    _unitOfWork.GetCommand<IExternalLoginComand>().Remove(_login);
                    await _unitOfWork.CommitAsync();
                }
            }
        }
        public async Task AddToRoleAsync(User user, string roleName)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: roleName.");

            using (var _unitOfWork = _factory.Create())
            {
                await _unitOfWork.GetCommand<IRoleComand>().Update(user.Id, roleName);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return await _userQuery.GetRoles(user.Id);
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");

            var roles = await _userQuery.GetRoles(user.Id);

            return roles.Contains(roleName);
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Argument cannot be null, empty, or whitespace: role.");

            using (var _unitOfWork = _factory.Create())
            {
                await _unitOfWork.GetCommand<IRoleComand>().Remove(user.Id, roleName);
                await _unitOfWork.CommitAsync();
            }
        }

        public async Task<string> GetPasswordHashAsync(User user)
        {
            return await Task.FromResult<string>(user.PasswordHash);
        }

        public async Task<bool> HasPasswordAsync(User user)
        {
            return await Task.FromResult<bool>(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public async Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
        }

        public async Task<string> GetSecurityStampAsync(User user)
        {
            return await Task.FromResult<string>(user.SecurityStamp);
        }

        public async Task SetSecurityStampAsync(User user, string stamp)
        {
            user.SecurityStamp = stamp;
        }
    }

    public interface IUserStore : IUserLoginStore<User, Guid>, IUserRoleStore<User, Guid>, IUserPasswordStore<User, Guid>, IUserSecurityStampStore<User, Guid>, IUserStore<User, Guid>, IDisposable
    {
        List<User> GetUsersByRole(int skip, int take, string role);
        List<Role> GetRolesbyId(Guid userId);
        Task<int> CountUsersByRole(string role);
    }
}