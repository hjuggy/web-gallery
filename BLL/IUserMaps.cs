﻿using Domain.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IUserMap
    {
        string GenresList2String(List<GenreSwitcher> favoriteGenresList);
        List<GenreSwitcher> GenresString2List(string favoriteGenres);
    }
}