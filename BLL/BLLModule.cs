﻿using Autofac;
using BLL.IdentityService;
using BLL.Services;
using BLL.ServicesFactory;
using Domain.Models;
using Microsoft.AspNet.Identity;
using System;

namespace BLL
{
    public class BLLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ImageService>().As<IImageService>();
            builder.RegisterType<VisitorService>().As<IVisitorService>();
            builder.RegisterType<RatingService>().As<IRatingService>();
            builder.RegisterType<CommentService>().As<ICommentService>();


            builder.RegisterType<UserStore>().As<IUserStore<User, Guid>>().InstancePerLifetimeScope();
            builder.RegisterType<UserStore>().As<IUserStore>();
            builder.RegisterType<RoleStore>().InstancePerLifetimeScope();

            builder.RegisterType<UserMap>().As<IUserMap>();
            builder.RegisterType<ServiceFactory>().As<IServiceFactory>();
        }
    }
}