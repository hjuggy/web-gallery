﻿using System;
using System.Threading.Tasks;
using System.Web;
using BLL.Services;
using BLL.ServicesFactory;
using Domain.Models;
using ExpressMapper.Extensions;
using Microsoft.AspNet.SignalR;
using WebGallery.Models;

namespace WebGallery.Hubs
{
    public class RatingHub : Hub
    {
        ServiceFactory factory = new ServiceFactory();

        public RatingHub()
        {
        }
        

        public async Task SendLike(string idimage)
        {
            var baseContext = Context.Request.GetHttpContext();
            string IpAddress = baseContext.Request.UserHostAddress;
            var _idimage = Guid.Parse(idimage);
            factory.Create<IRatingService>().TouchLike(IpAddress, _idimage);
            var image = await factory.Create<IImageService>().GetById(_idimage);
            string like = image.Like.ToString();
            string dislike = image.DisLike.ToString();
            Clients.All.updateLike(like, idimage);
            Clients.All.updateDisLike(dislike, idimage);
        }

        public async Task SendDisLike(string idimage)
        {
            var baseContext = Context.Request.GetHttpContext();
            string IpAddress = baseContext.Request.UserHostAddress;
            var _idimage = Guid.Parse(idimage);
            factory.Create<IRatingService>().TouchDisLike(IpAddress, _idimage);
            var image = await factory.Create<IImageService>().GetById(_idimage);
            string like = image.Like.ToString();
            Clients.All.updateLike(like, idimage);
            string dislike = image.DisLike.ToString();
            Clients.All.updateDisLike(dislike, idimage); 
        }

        public void SendComment(string WriterName, string WriterId, string ImageId, string CommentContent)
        {
            var comment = new CommentDisplay();
            comment.CommentContent = CommentContent;
            comment.WriterName = WriterName;
            comment.WriterId = Guid.Parse(WriterId);
            comment.ImageId = Guid.Parse(ImageId);
            comment.Id = Guid.NewGuid();
            comment.DateTimeComment = DateTime.Now;
            factory.Create<ICommentService>().Add(comment.Map<CommentDisplay, Comment>());
            var name = comment.WriterName;
            var content = comment.CommentContent;
            var datetime = comment.DateTimeComment.ToString("U");
            Clients.All.updateComment(name, datetime, content);
        }
    }
}