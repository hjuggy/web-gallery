﻿using Autofac;
using Autofac.Integration.Mvc;
using DAL;
using BLL;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using Microsoft.AspNet.Identity;
using Domain.Models;
using System;
using ExpressMapper;
using DAL.Entities;
using Domain;
using BLL.IdentityService;

namespace WebGallery
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //autofac
            var builder = new ContainerBuilder();
            builder.RegisterType<UserManager<User, Guid>>().As<UserManager<User, Guid>>().InstancePerRequest();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<BLLModule>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));


            Mapper.Register<UserEntity, User>().Member(dest => dest.Id, src => src.Id)
                                               .Member(dest => dest.UserName, src => src.UserName)
                                               .Member(dest => dest.IsOver18, src => src.IsOver18)
                                               .Member(dest => dest.PasswordHash, src => src.PasswordHash)
                                               //.Member(dest => dest.Roles, src => src.Roles)
                                               .Member(dest => dest.SecurityStamp, src => src.SecurityStamp);


            Mapper.Register<ImageEntity, ImageDescript>().Member(dest => dest.Id, src => src.Id)
                                                         .Member(dest => dest.AuthorImageId, src => src.AuthorImageId)
                                                         .Member(dest => dest.DateTimeLoad, src => src.DateTimeLoad)
                                                         .Member(dest => dest.Description, src => src.Description)
                                                         .Member(dest => dest.Extension, src => src.Extension)
                                                         .Member(dest => dest.Genre, src => src.Genre)
                                                         .Member(dest => dest.IsPrivate, src => src.IsPrivate)
                                                         .Member(dest => dest.Name, src => src.Name)
                                                         .Member(dest => dest.Like, src => src.Likes.Count)
                                                         .Member(dest => dest.DisLike, src => src.DisLikes.Count);

            Mapper.Register<CommentEntity, Comment>().Member(dest => dest.Id, src => src.Id)
                                                     .Member(dest => dest.CommentContent, src => src.CommentContent)
                                                     .Member(dest => dest.DateTimeComment, src => src.DateTimeComment)
                                                     .Member(dest => dest.Id, src => src.Id)
                                                     .Member(dest => dest.ImageId, src => src.ImageId)
                                                     .Member(dest => dest.WriterId, src => src.WriterId)
                                                     .Member(dest => dest.WriterName, src => src.Writer.UserName);


            Mapper.Register<RoleEntity, Role>().Member(dest => dest.Id, src => src.Id)
                                               .Member(dest => dest.Name, src => src.Name);
                                               //.Member(dest => dest.Users, src => src.Users);
         



        }
    }
}
