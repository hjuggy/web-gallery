﻿
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        }
        else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});

function FormSearchSubmit() {
    var Search = $(".searcher-key").val();
    if (Search == "") {
    } else {
        $("#FormSearch").submit();
    }
}

$(function () {
    $(".searcher-key").keyup(function () {
        var Search = $(".searcher-key").val();
        if (Search == "") {
            var element = document.getElementById("result");
            while (element.firstChild) {
                element.removeChild(element.firstChild);
            }
        }
        else {
            $.ajax({
                type: "post",
                url: "/Image/Find",
                data: { Search: Search },
                success: function (r) {
                    var element = document.getElementById("result");
                    while (element.firstChild) {
                        element.removeChild(element.firstChild);
                    }

                    if (r[0] != null) {
                        for (var i = 0; i < r.length; i++) {
                            var result = document.getElementById("result");
                            var div = document.createElement('div');
                            div.setAttribute('class', "searchlist")
                            var newlink = document.createElement('a');
                            var link = "/Image/Details?id=" + r[i].Id;
                            newlink.setAttribute('href', link);
                            newlink.insertAdjacentText('afterbegin', r[i].Name);
                            div.appendChild(newlink);
                            result.appendChild(div);
                        }
                    }
                }
            });
            return false;
        }
    });
});

function getElementsWithAttributeValue(attribute, value) {
    var matchingElements = [];
    var allElements = document.getElementsByTagName('*');
    for (var i = 0, n = allElements.length; i < n; i++) {
        if (allElements[i].getAttribute(attribute) === value) {

            matchingElements.push(allElements[i]);
        }
    }
    return matchingElements;
};

$(function () {
    var RRRating = $.connection.ratingHub;
    RRRating.client.updateLike = function (like, idimage) {
        var LikeElms = getElementsWithAttributeValue('likeidimage', idimage)[0];
        $(LikeElms).fadeOut(function () {
            $(this).text(like);
            $(this).fadeIn();
        });
    };

    RRRating.client.updateComment = function (name, datetime, content) {
        var divComent = document.createElement('div');
        var divName = document.createElement('div');
        var divDateTime = document.createElement('div');
        var divContent = document.createElement('div');
        var elmBR = document.createElement('br');
        var elmBR1 = document.createElement('br');
        divName.setAttribute('class', "toLeft");
        divName.append(name);
        divDateTime.setAttribute('class', "toRight");
        divDateTime.append(datetime);
        divContent.append(content);
        divComent.appendChild(elmBR1);
        divComent.prepend(divContent);
        divComent.prepend(elmBR);
        divComent.prepend(divDateTime);
        divComent.prepend(divName);
        $("#coments").prepend(divComent);
    };

    RRRating.client.updateDisLike = function (dislike, idimage) {
        var disLikeElms = getElementsWithAttributeValue('dislikeidimage', idimage)[0];
        $(disLikeElms).fadeOut(function () {
            $(this).text(dislike);
            $(this).fadeIn();
        });
    };

    RRRating.client.updateNew = function (selector) {
        var itemsand = getElementsWithAttributeValue('idgenry', selector)[0];
        $(itemsand).fadeOut(function () {
            $(this).text("NEW");
            $(this).fadeIn();
        });
    };

    $.connection.hub.start().done(function () {
        $('a.like').on("click", function () {
            var idimage = $(this).attr("idimage");
            RRRating.server.sendLike(idimage);
        });


        $('a.dislike').on("click", function () {
            var idimage = $(this).attr("idimage");
            RRRating.server.sendDisLike(idimage);
        });

        $('#btnCreateComment').on("click", function () {
            if ($("#CommentText").val()!=="") {
                var comment = $('#CreateComment').serializeArray();
                var WriterId = comment[0].value;
                var WriterName = comment[1].value;
                var ImageId = comment[2].value;
                var CommentContent = comment[3].value;
                RRRating.server.sendComment(WriterName, WriterId, ImageId, CommentContent);
                $("#CommentText").val("");
                $("#CommentText").focus();
            }
        });
    });
});


function nextpage(e) {
    e.preventDefault();
    var CommentsList = $('#CommentsList').serializeArray();
    var Pagestr = CommentsList[0].value;
    var ImageId = CommentsList[1].value;
    var Page = Number(Pagestr) + 1;
    $.ajax({
        type: "get",
        url: "/Image/Comments",
        dataType: 'json',
        data: {
            "id": ImageId,
            "page": Page
        },
        success: function (r) {
            if (r != undefined) {
                var comments_div = document.getElementById("coments");
                while (comments_div.firstChild) {
                    comments_div.removeChild(comments_div.firstChild);
                }

                var comments = r.Comments;
                for (var i = 0; i < comments.length; i++) {
                    var divComent = getcoment(comments[i].WriterName, comments[i].DateTimeComment, comments[i].CommentContent);
                    $("#coments").append(divComent);
                }

                var divlink = getlink(r.Page, r.MaxPage);
                $("#coments").append(divlink);

                $("#CarentPage").attr("value", r.Page);
            }
        }
    });
}

function previoupage(e) {
    e.preventDefault();
    var CommentsList = $('#CommentsList').serializeArray();
    var Pagestr = CommentsList[0].value;
    var ImageId = CommentsList[1].value;
    var Page = Number(Pagestr) - 1;
    $.ajax({
        type: "get",
        url: "/Image/Comments",
        dataType: 'json',
        data: {
            "id": ImageId,
            "page": Page
        },
        success: function (r) {
            if (r != undefined) {
                var comments_div = document.getElementById("coments");
                while (comments_div.firstChild) {
                    comments_div.removeChild(comments_div.firstChild);
                }

                var comments = r.Comments;
                for (var i = 0; i < comments.length; i++) {
                    var divComent = getcoment(comments[i].WriterName, comments[i].DateTimeComment, comments[i].CommentContent);
                    $("#coments").append(divComent);
                }

                var divlink = getlink(r.Page, r.MaxPage);
                $("#coments").append(divlink);

                $("#CarentPage").attr("value", r.Page);
            }
        }
    });
}

function parseJsonDate(jsonDateString) {
    return new Date(parseInt(jsonDateString.replace('/Date(', '')));
}

function getcoment(WriterName, DateTimeComment, CommentContent) {
    var divComent = document.createElement('div');
    var divName = document.createElement('div');
    var divDateTime = document.createElement('div');
    var divContent = document.createElement('div');
    var elmBR = document.createElement('br');
    var elmBR1 = document.createElement('br');
    divName.setAttribute('class', "toLeft");
    divName.append(WriterName);
    divDateTime.setAttribute('class', "toRight");
    divDateTime.append(parseJsonDate(DateTimeComment).toLocaleString());
    divContent.append(CommentContent);
    divComent.appendChild(elmBR1);
    divComent.prepend(divContent);
    divComent.prepend(elmBR);
    divComent.prepend(divDateTime);
    divComent.prepend(divName);
    return divComent;
}

function getlink(Page, MaxPage) {
    var divlink = document.createElement('div');
    if (Page == 1 & MaxPage>1) {
        var anext = document.createElement('a');
        anext.setAttribute("href", "");
        anext.setAttribute("id", "next");
        anext.setAttribute("onclick", "nextpage(event)");
        anext.setAttribute("class", "toRight");
        anext.append("Следующие");
        divlink.appendChild(anext);
    } else if (Page == MaxPage & MaxPage>1) {
        var aprevious = document.createElement('a');
        aprevious.setAttribute("href", "");
        aprevious.setAttribute("id", "previous");
        aprevious.setAttribute("onclick", "previoupage(event)");
        aprevious.setAttribute("class", "toLeft");
        aprevious.append("Предыдущие ");
        divlink.appendChild(aprevious);
    } else if (Page > 1 & Page < MaxPage) {
        var aprevious = document.createElement('a');
        aprevious.setAttribute("href", "");
        aprevious.setAttribute("id", "previous");
        aprevious.setAttribute("onclick", "previoupage(event)");
        aprevious.setAttribute("class", "toLeft");
        aprevious.append("Предыдущие ");
        var anext = document.createElement('a');
        anext.setAttribute("href", "");
        anext.setAttribute("id", "next");
        anext.setAttribute("onclick", "nextpage(event)");
        anext.setAttribute("class", "toRight");
        anext.append("Следующие");
        divlink.appendChild(aprevious);
        divlink.appendChild(anext);
    }
    return divlink;
}



function ToBan( id) {
    $.ajax({
        type: "post",
        url: "/Administration/ToBan",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("В бане");
                btn.appendChild(div);
            }
        }
    });
    return false;
}

function AddModerator(id) {
    $.ajax({
        type: "post",
        url: "/Administration/AddModerator",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("Модератор");
                btn.appendChild(div);
            }
        }
    });
    return false;
}

function RemoveBan(id) {
    $.ajax({
        type: "post",
        url: "/Administration/RemoveBan",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("Разбанен");
                btn.appendChild(div);
            }
        }
    });
    return false;
}

function RemoveModerator(id) {
    $.ajax({
        type: "post",
        url: "/Administration/RemoveModerator",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("Не модератор");
                btn.appendChild(div);
            }
        }
    });
    return false;
}


DelImg

function DelImg(id) {
    $.ajax({
        type: "post",
        url: "/Administration/RemoveImage",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("Удалено");
                btn.appendChild(div);
            }
        }
    });
    return false;
}


function EditDiscript() {
    document.getElementById('DescriptionIMG').disabled = false;
    document.getElementById('btnSaveDescript').disabled = false;
    document.getElementById('btnEditDescript').disabled = true;
}

function SaveDescript(id) {
    var description = $("#DescriptionIMG").val();
    $.ajax({        
        type: "post",
        url: "/Administration/SaveDescript",
        dataType: 'json',
        data: {
            "id": id,
            "description": description
        },
        success: function (r) {
            if (r === 200) {
                document.getElementById('DescriptionIMG').disabled = true;
                document.getElementById('btnSaveDescript').disabled = true;
                document.getElementById('btnEditDescript').disabled = false;
            }
        }
    });
    return false;
}














function nextpageA(e) {
    e.preventDefault();
    var CommentsList = $('#CommentsList').serializeArray();
    var Pagestr = CommentsList[0].value;
    var ImageId = CommentsList[1].value;
    var Page = Number(Pagestr) + 1;
    $.ajax({
        type: "get",
        url: "/Image/Comments",
        dataType: 'json',
        data: {
            "id": ImageId,
            "page": Page
        },
        success: function (r) {
            if (r != undefined) {
                var comments_div = document.getElementById("coments");
                while (comments_div.firstChild) {
                    comments_div.removeChild(comments_div.firstChild);
                }

                var comments = r.Comments;
                for (var i = 0; i < comments.length; i++) {
                    var divComent = getcomentA(comments[i].WriterName, comments[i].DateTimeComment, comments[i].CommentContent, comments[i].Id);
                    $("#coments").append(divComent);
                }

                var divlink = getlinkA(r.Page, r.MaxPage);
                $("#coments").append(divlink);

                $("#CarentPage").attr("value", r.Page);
            }
        }
    });
}

function previoupageA(e) {
    e.preventDefault();
    var CommentsList = $('#CommentsList').serializeArray();
    var Pagestr = CommentsList[0].value;
    var ImageId = CommentsList[1].value;
    var Page = Number(Pagestr) - 1;
    $.ajax({
        type: "get",
        url: "/Image/Comments",
        dataType: 'json',
        data: {
            "id": ImageId,
            "page": Page
        },
        success: function (r) {
            if (r != undefined) {
                var comments_div = document.getElementById("coments");
                while (comments_div.firstChild) {
                    comments_div.removeChild(comments_div.firstChild);
                }

                var comments = r.Comments;
                for (var i = 0; i < comments.length; i++) {
                    var divComent = getcomentA(comments[i].WriterName, comments[i].DateTimeComment, comments[i].CommentContent, comments[i].Id);
                    $("#coments").append(divComent);
                }

                var divlink = getlinkA(r.Page, r.MaxPage);
                $("#coments").append(divlink);

                $("#CarentPage").attr("value", r.Page);
            }
        }
    });
}



function getcomentA(WriterName, DateTimeComment, CommentContent, Id) {
    var divComent = document.createElement('div');
    divComent.setAttribute('idbtn', Id);
    var divName = document.createElement('div');
    var divDateTime = document.createElement('div');
    var divContent = document.createElement('div');
    var divbtn = document.createElement('div');
    var inputelm = document.createElement('input');  
    inputelm.setAttribute("type", "button");
    inputelm.setAttribute("value", "Удалить");
    var stratr = "DelComment(\'" + Id + "\')";
    inputelm.setAttribute("onclick", stratr);
    divbtn.append(inputelm);
    var elmBR = document.createElement('br');
    var elmBR1 = document.createElement('br');
    divName.setAttribute('class', "toLeft");
    divName.append(WriterName);
    divDateTime.setAttribute('class', "toRight");
    divDateTime.append(parseJsonDate(DateTimeComment).toLocaleString());
    divContent.append(CommentContent);
    divComent.appendChild(elmBR1);
    divComent.prepend(divContent);
    divComent.prepend(elmBR);
    divComent.prepend(divDateTime);
    divComent.prepend(divName);
    divComent.append(divbtn);
    return divComent;
}

function getlinkA(Page, MaxPage) {
    var divlink = document.createElement('div');
    if (Page == 1 & MaxPage > 1) {
        var anext = document.createElement('a');
        anext.setAttribute("href", "");
        anext.setAttribute("id", "next");
        anext.setAttribute("onclick", "nextpageA(event)");
        anext.setAttribute("class", "toRight");
        anext.append("Следующие");
        divlink.appendChild(anext);
    } else if (Page == MaxPage & MaxPage > 1) {
        var aprevious = document.createElement('a');
        aprevious.setAttribute("href", "");
        aprevious.setAttribute("id", "previous");
        aprevious.setAttribute("onclick", "previoupageA(event)");
        aprevious.setAttribute("class", "toLeft");
        aprevious.append("Предыдущие ");
        divlink.appendChild(aprevious);
    } else if (Page > 1 & Page < MaxPage) {
        var aprevious = document.createElement('a');
        aprevious.setAttribute("href", "");
        aprevious.setAttribute("id", "previous");
        aprevious.setAttribute("onclick", "previoupageA(event)");
        aprevious.setAttribute("class", "toLeft");
        aprevious.append("Предыдущие ");
        var anext = document.createElement('a');
        anext.setAttribute("href", "");
        anext.setAttribute("id", "next");
        anext.setAttribute("onclick", "nextpageA(event)");
        anext.setAttribute("class", "toRight");
        anext.append("Следующие");
        divlink.appendChild(aprevious);
        divlink.appendChild(anext);
    }
    return divlink;
}


DelComment

function DelComment(id) {
    $.ajax({
        type: "post",
        url: "/Administration/RemoveComment",
        dataType: 'json',
        data: {
            "id": id
        },
        success: function (r) {
            if (r === 200) {
                var btn = getElementsWithAttributeValue("idbtn", id)[0];
                while (btn.firstChild) {
                    btn.removeChild(btn.firstChild);
                }
                var div = document.createElement('div');
                div.append("Удалено");
                btn.appendChild(div);
            }
        }
    });
    return false;
}