﻿
function activOn(e) {
    document.getElementById('Genre1').disabled = e.checked;
    document.getElementById('Genre2').disabled = !(e.checked);
}

var listen = function (element, event, fn) {
    return element.addEventListener(event, fn, false);
};

listen(document, 'DOMContentLoaded', function () {

    var fileInput = document.querySelector('#file-input');
    var listView = document.querySelector('#list-view');

    listen(fileInput, 'change', function (event) {
        var file = fileInput.files;
        for (var i = 0; i < listView.childElementCount; i++) {
            listView.childNodes[i].remove();
        }
        generatePreview(file[0]);
    });

    var generatePreview = function (file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var dataUrl = e.target.result;
            var div = document.createElement('div');
            var image = new Image();
            image.className = "PreviewImage";
            image.onload = function () {
            };
            image.src = dataUrl;
            div.appendChild(image);
            listView.appendChild(div);
        };
        reader.readAsDataURL(file);
    };
})