﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(WebGallery.Startup))]

namespace WebGallery
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
