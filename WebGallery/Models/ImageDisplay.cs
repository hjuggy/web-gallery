﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebGallery.Models
{
    public class ImageDisplay
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public string Genre { get; set; }

        public string Description { get; set; }

        public bool IsPrivate { get; set; }

        public DateTime DateTimeLoad { get; set; }

        public Guid AuthorImageId { get; set; }

        public int Like { get; set; }

        public int DisLike { get; set; }
    }
}
