﻿using System.Collections.Generic;


namespace WebGallery.Models
{
    public class FindListDisplay
    {
        public IEnumerable<ImageDisplay> Images { get; set; }
        public string Search { get; set; }
        public PageInfo Page { get; set; }
    }
}