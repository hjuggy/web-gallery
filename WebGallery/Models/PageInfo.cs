﻿using System;

namespace WebGallery.Models
{
    public class PageInfo
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }

        public int SkipItems => ItemsPerPage * (CurrentPage - 1);

        public int TotalPages => TotalItems % ItemsPerPage == 0 ? TotalItems / ItemsPerPage : TotalItems / ItemsPerPage + 1;
    }
}