﻿using System.Collections.Generic;

namespace WebGallery.Models
{
    public class StartPageDispay<TImage>
    {
        public string Genre { get; set; }
        public IEnumerable<TImage> Images { get; set; }
        public int NumberOfImages { get; set; }
    }
}
