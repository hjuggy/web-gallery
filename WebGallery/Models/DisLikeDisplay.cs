﻿using System;

namespace WebGallery.Models
{
    public class DisLikeDisplay
    {
        public Guid ImageId { get; set; }

        public string UserIP { get; set; }
    }
}