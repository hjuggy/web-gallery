﻿using System;
using System.ComponentModel.DataAnnotations;


namespace WebGallery.Models
{
    public class CommentDisplay
    {
        public  Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public string WriterName { get; set; }

        public Guid WriterId { get; set; }

        public DateTime DateTimeComment { get; set; }

        [Required(ErrorMessage = "Введите свой комментарий")]
        public string CommentContent { get; set; }
    }
 }