﻿using System.Collections.Generic;

namespace WebGallery.Models
{
    public class SortListDisplay
    {
        public IEnumerable<ImageDisplay> Images { get; set; }
        public PageInfo Page { get; set; }
    }
}