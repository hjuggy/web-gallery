﻿using Domain.Models;
using System;
using System.Collections.Generic;

namespace WebGallery.Models
{
    public class EditUserDisplay
    {
        public Guid Id { get; set; }
        public bool IsOver18 { get; set; }
        public int ImageTake { get; set; } 
        public List<GenreSwitcher> FavoriteGenresList { get; set; }
    }
}