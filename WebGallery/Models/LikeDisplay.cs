﻿using System;


namespace WebGallery.Models
{
    public class LikeDisplay
    {
        public Guid ImageId { get; set; }

        public string UserIP { get; set; }
    }
}