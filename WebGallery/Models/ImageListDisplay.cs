﻿using System.Collections.Generic;


namespace WebGallery.Models
{
    public class ImageListDisplay
    {
        public IEnumerable<ImageDisplay> Images { get; set; }
        public string CurrentGenre { get; set; }
        public PageInfo Page { get; set; }

    }
}