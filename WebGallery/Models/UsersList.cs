﻿using Domain.Models;
using System.Collections.Generic;

namespace WebGallery.Models
{
    public class UsersList
    {
        public List<User> Users { get; set; }
        public int Page { get; set; }
        public int MaxPage { get; set; }
    }
}