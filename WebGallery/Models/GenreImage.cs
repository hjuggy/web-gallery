﻿namespace WebGallery.Models
{
    public class GenreDisplay
    {
        public string Genre { get; set; }
        public int NumberOfImages { get; set; }
    }
}
