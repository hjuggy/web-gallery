﻿namespace WebGallery.Models
{
    public class ImageLoad
    {
        public string Name { get; set; }

        public string Genre { get; set; }

        public string Description { get; set; }

        public bool IsPrivate { get; set; }

        //public int Like { get; set; }

        //public int DisLike { get; set; }
    }
}