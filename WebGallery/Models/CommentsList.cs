﻿using System;
using System.Collections.Generic;

namespace WebGallery.Models
{
    public class CommentsList
    {
        public Guid ImageId { get; set; }
        public List<CommentDisplay> Comments { get; set; }
        public int Page { get; set; }
        public int MaxPage { get; set; }
    }
}