﻿using System.ComponentModel.DataAnnotations;

namespace WebGallery.Models
{
    public class RegisterUserDisplay
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Длина {0} должна быть не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Пороль не совпадает")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Я совершеннолетний")]
        public bool IsOver18 { get; set; }

    }

}