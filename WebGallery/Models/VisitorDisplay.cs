﻿using System;
using System.Collections.Generic;

namespace WebGallery.Models
{
    public class VisitorDisplay
    {
        public Guid Id { get; set; }
        public string IP { get; set; }
    }
}
