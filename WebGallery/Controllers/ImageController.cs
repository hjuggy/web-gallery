﻿using BLL.IdentityService;
using BLL.Services;
using BLL.ServicesFactory;
using DAL.Entities;
using Domain.Models;
using ExpressMapper.Extensions;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebGallery.Models;

namespace WebGallery.Controllers
{
    public class ImageController : Controller
    {
        UserManager<User,Guid> _userManager;
        IImageService _imageService;
        ICommentService _commentService;
        IUserStore _userStore;
        public ImageController(IImageService imageService, ICommentService commentService, UserManager<User, Guid> userManager ,IUserStore userStore)
        {
            _imageService = imageService;
            _commentService = commentService;
            _userManager = userManager;
            _userStore = userStore;
        }
               
        public async Task<ActionResult> Index(string genre = null, int page = 1)
        {

            if (genre != null)
            {
                PageInfo pageInfo = new PageInfo()
                {
                    CurrentPage = page,
                    ItemsPerPage = 20,
                    TotalItems = await _imageService.CountImagesInGenre(genre),
                    
                };
                ImageListDisplay imageList = new ImageListDisplay()
                {
                    CurrentGenre = genre,
                    Page = pageInfo,
                    Images = (await _imageService.GetByGenre(genre, pageInfo.ItemsPerPage, pageInfo.SkipItems))
                                                 .Map<IEnumerable<ImageDescript>, IEnumerable<ImageDisplay>>(),
                };
                return View(imageList);
            }
            return View("ViewStart");
        }

        public PartialViewResult PartialViewMenu()
        {
            Guid idUser = Guid.Empty;
            if (Request.IsAuthenticated)
            {
                idUser = Guid.Parse(User.Identity.GetUserId());
            }
            var resault = _imageService.GetGenres(idUser);
            return PartialView("PartialViewMenu", resault.Map<IEnumerable<GenreImage>, IEnumerable<GenreDisplay>>());
        }

        public PartialViewResult PartialViewGenry()
        {
            var resault = _imageService.GetGenres(Guid.Empty);
            return PartialView("PartialViewGenry", resault.Map<IEnumerable<GenreImage>, IEnumerable<GenreDisplay>>());
        }

        public PartialViewResult PartialViewAll()
        {
            Guid idUser = Guid.Empty;
            if (Request.IsAuthenticated)
            {
                idUser = Guid.Parse(User.Identity.GetUserId());
            }
            var _images = _imageService.GetImgOfStartPage(idUser);

            return PartialView("PartialViewAll", _images.Map<List<ImgOfStartPage<ImageDescript>>, List<StartPageDispay<ImageDisplay>>>());
        }

        public ActionResult ImageAllGenry()
        {
            var _images = _imageService.GetImgOfStartPage(Guid.Empty);

            return View("ImageAllGenry", _images.Map<List<ImgOfStartPage<ImageDescript>>, List<StartPageDispay<ImageDisplay>>>());
        }


        public PartialViewResult PartialComment(Guid id, int page=1)
        {
            return PartialView("PartialComment", GetCommentsList(id, page));
        }

        [HttpGet]
        public JsonResult Comments(string id, int page = 1)
        {
            var _id = Guid.Parse(id);
            var comments = GetCommentsList(_id, page);
            return Json(comments, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            return View((await _imageService.GetById(id)).Map<ImageDescript, ImageDisplay>());
        }

        [HttpPost]
        public async Task<JsonResult> Find(string Search)
        {
            var images = (await _imageService.Find(Search, 10)).Select(p => new { p.Name, p.Path, p.Id });
            return Json(images, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> FindPage(string search, int page = 1)
        {
            if (search != null | search != "")
            {
                PageInfo pageInfo = new PageInfo()
                {
                    CurrentPage = page,
                    ItemsPerPage = 20,
                    TotalItems = await _imageService.CountImgFind(search),
                };
                FindListDisplay findList = new FindListDisplay()
                {
                    Search = search,
                    Page = pageInfo,
                    Images = (await _imageService.Find(search, pageInfo.ItemsPerPage, pageInfo.SkipItems))
                                                 .Map<IEnumerable<ImageDescript>, IEnumerable<ImageDisplay>>(),
                };
                return View(findList);
            }
            return View("ViewStart");
        }

        public async Task<ActionResult> PopularImages(int page = 1)
        {

            PageInfo pageInfo = new PageInfo()
            {
                CurrentPage = page,
                ItemsPerPage = 20,
                TotalItems = await _imageService.CountImage(),
            };
            SortListDisplay imageList = new SortListDisplay()
            {

                Page = pageInfo,
                Images = (await _imageService.PopularImage( pageInfo.ItemsPerPage, pageInfo.SkipItems))
                                             .Map<IEnumerable<ImageDescript>, IEnumerable<ImageDisplay>>(),
            };
            return View(imageList);
        }

        public async Task<ActionResult> NewImages(int page = 1)
        {
            var oneDayAgo = DateTime.Now.AddDays(-1.01);
            PageInfo pageInfo = new PageInfo()
            {
                CurrentPage = page,
                ItemsPerPage = 20,
                TotalItems = await _imageService.CountSortImage(p => p.DateTimeLoad > oneDayAgo),
            };
            SortListDisplay imageList = new SortListDisplay()
            {
                Page = pageInfo,
                Images = (await _imageService.SortImage(p => p.DateTimeLoad > oneDayAgo, pageInfo.ItemsPerPage, pageInfo.SkipItems))
                                             .Map<IEnumerable<ImageDescript>, IEnumerable<ImageDisplay>>(),
            };
            return View(imageList);
        }

        public PartialViewResult PartialCommentForm(Guid id)
        {
            var _Id = Guid.Parse(User.Identity.GetUserId());
            var roles = _userStore.GetRolesbyId(_Id);
            if (roles.Any(r => r.Name == "Ban"))
            {
                return PartialView("PartialViewEmpty");
            }
            return PartialView("PartialCommentForm", id);
        }

        private CommentsList GetCommentsList(Guid id, int page = 1)
        {
            int take = 10;
            int skip = (page - 1) * take;
            var comments = _commentService.GetComments(id, take, skip);
            var commentsList = new CommentsList();
            commentsList.Comments = comments.Map<List<Comment>, List<CommentDisplay>>();
            commentsList.ImageId = id;
            var countComments = _commentService.CountComments(id);
            commentsList.MaxPage = (countComments % take) == 0 ? countComments / take : countComments / take + 1;
            commentsList.Page = page;
            return commentsList;
        }
    }
}
