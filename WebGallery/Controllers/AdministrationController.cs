﻿using BLL.IdentityService;
using BLL.Services;
using Domain.Models;
using ExpressMapper.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebGallery.Hubs;
using WebGallery.Models;

namespace WebGallery.Controllers
{
    public class AdministrationController : Controller
    {
        private readonly UserManager<User, Guid> _userManager;
        private IUserStore _userStore;
        private IImageService _imageService;
        private ICommentService _commentService;

        public AdministrationController(UserManager<User, Guid> userManager, IUserStore userStore, IImageService imageService ,ICommentService commentService)
        {
            _userManager = userManager;
            _userStore = userStore;
            _imageService = imageService;
            _commentService = commentService;
        }

        public async Task<ActionResult> ControlUsers(int page = 1)
        {
            if (IsRight(moderator: "Moderator", admin: "Admin"))
            {
                var users = await GetUsersListByRole(page, "User");
                return View(users);
            }
            else
            {
                return View("NotRight");
            }
        }

        public ActionResult Create()
        {
            if (IsRight(moderator: "Moderator", admin: "Admin"))
            {
                ViewBag.Genres = _imageService.GetGenres(Guid.Empty);
                return View();
            }
            else
            {
                return View("NotRight");
            }
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload, ImageLoad imageLoad)
        {
            if (upload != null)
            {
                ImageDisplay imageDisplay = imageLoad.Map<ImageLoad, ImageDisplay>();
                imageDisplay.AuthorImageId = Guid.Parse(User.Identity.GetUserId());
                imageDisplay.Id = Guid.NewGuid();
                imageDisplay.DateTimeLoad = DateTime.Now;
                string extension = Path.GetExtension(upload.FileName);
                imageDisplay.Path = GetPath(imageDisplay.Id.ToString(), extension);
                upload.SaveAs(Server.MapPath(imageDisplay.Path));
                _imageService.Add(imageDisplay.Map<ImageDisplay, ImageDescript>());
                SendMessage(imageDisplay.Genre);
            }

            return RedirectToAction("Create");
        }
        
        public async Task<ActionResult> ImagesImgur()
        {
            ///Client-ID 41b4696307353f2
            List<ImageImgur> imageImgurs = new List<ImageImgur>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Client-ID 41b4696307353f2");
                HttpResponseMessage response = await client.GetAsync("https://api.imgur.com/3/gallery/t/cat");
                response.EnsureSuccessStatusCode();
                var rbody = await response.Content.ReadAsAsync<RootObject>();

                imageImgurs = rbody.data.items.Where(t => t.images != null).SelectMany(p => p.images).ToList();
            }

            return View(imageImgurs);
        }
                       
        [HttpPost]
        public async Task<string> ToBan(string id)
        {
            var _id = Guid.Parse(id);
            var user = await _userStore.FindByIdAsync(_id);
            await _userStore.AddToRoleAsync(user, "Ban");
            await _userStore.RemoveFromRoleAsync(user, "User");
            return "200";
        }
        
        [HttpPost]
        public async Task<string> AddModerator(string id)
        {
            var _id = Guid.Parse(id);
            var user = await _userStore.FindByIdAsync(_id);
            await _userStore.AddToRoleAsync(user, "Moderator");
            await _userStore.RemoveFromRoleAsync(user, "User");
            return "200";
        }

        public async Task<ActionResult> ControlBanUsers(int page = 1)
        {
            if (IsRight(moderator: "Moderator", admin: "Admin"))
            {
                var users = await GetUsersListByRole(page, "Ban");
                return View(users);
            }
            else
            {
                return View("NotRight");
            }
        }

        [HttpPost]
        public async Task<string> RemoveBan(string id)
        {
            var _id = Guid.Parse(id);
            var user = await _userStore.FindByIdAsync(_id);
            await _userStore.RemoveFromRoleAsync(user, "Ban");
            await _userStore.AddToRoleAsync(user, "User");
            return "200";
        }

        public async Task<ActionResult> ControlModerators(int page = 1)
        {
            if (IsRight(admin: "Admin"))
            {
                var users = await GetUsersListByRole(page, "Moderator");
                return View(users);
            }
            else
            {
                return View("NotRight");
            }
        }
        
        [HttpPost]
        public async Task<string> RemoveModerator(string id)
        {
            var _id = Guid.Parse(id);
            var user = await _userStore.FindByIdAsync(_id);
            await _userStore.RemoveFromRoleAsync(user, "Moderator");
            await _userStore.AddToRoleAsync(user, "User");
            return "200";
        }

        public async Task<ActionResult> ModerationImage(int page = 1, string genre = null)
        {
            if (IsRight(moderator: "Moderator", admin: "Admin"))
            {
                var images = await GetImagesListByGenre(page, genre);
                return View(images); 
            }
            else
            {
                return View("NotRight");
            }
        }
        
        public async Task<ActionResult> EditImage(Guid id)
        {
            if (IsRight(moderator:"Moderator", admin: "Admin"))
            {
                return View((await _imageService.GetById(id)).Map<ImageDescript, ImageDisplay>());
            }
            else
            {
                return View("NotRight");
            }
        }

        [HttpPost]
        public async Task<string> RemoveImage(string id)
        {
            var _id = Guid.Parse(id);
            await _imageService.Remove(_id);
            return "200";
        }

        [HttpPost]
        public async Task<string> SaveDescript(string id, string description)
        {
            var _id = Guid.Parse(id);
            await _imageService.SaveDescript(_id, description);
            return "200";
        }

        public PartialViewResult PartialComment(Guid id, int page = 1)
        {
            return PartialView("PartialComment", GetCommentsList(id, page));
        }

        [HttpPost]
        public async Task<string> RemoveComment(string id)
        {
            var _id = Guid.Parse(id);
            await _commentService.Remove(_id);
            return "200";
        }

        public PartialViewResult PartialViewModerator()
        {
            var _Id= Guid.Parse(User.Identity.GetUserId());
            var roles = _userStore.GetRolesbyId(_Id);
            if (roles.Any(r=>r.Name=="Ban"))
            {
                return PartialView("PartialViewEmpty");
            }
            else if (roles.Any(r => r.Name == "Moderator" | r.Name == "Admin"))
            {
                return PartialView("PartialViewModerator");
            }
            return PartialView("PartialViewEmpty");
        }

        public PartialViewResult PartialViewAdmin()
        {
            var _Id = Guid.Parse(User.Identity.GetUserId());
            var roles = _userStore.GetRolesbyId(_Id);
            if (roles.Any(r => r.Name == "Ban"))
            {
                return PartialView("PartialViewEmpty");
            }
            else if (roles.Any(r => r.Name == "Admin"))
            {
                return PartialView("PartialViewAdmin");
            }
            return PartialView("PartialViewEmpty");
        }

        private bool IsRight(string moderator= "NotModerator",string admin= "NotAdmin")
        {
            var _Id = Guid.Parse(User.Identity.GetUserId());
            var roles = _userStore.GetRolesbyId(_Id);
            if (roles.Any(r => r.Name == moderator || r.Name == admin))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private async Task<ImageListDisplay> GetImagesListByGenre(int page = 1, string genre = null)
        {
            int take = 40;
            int skip = (page - 1) * take;
            var images = (await _imageService.GetByGenre(genre, take, skip)).Map<IEnumerable<ImageDescript>, IEnumerable<ImageDisplay>>();
            var count = await _imageService.CountImagesInGenre(genre);

            var imageslist = new ImageListDisplay
            {
                CurrentGenre = genre,
                Images = images,
                Page = new PageInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = take,
                    TotalItems = count,
                },
            };
            return imageslist;
        }

        private async Task<UsersList> GetUsersListByRole(int page, string role)
        {
            int take = 40;
            int skip = (page - 1) * take;
            var users = _userStore.GetUsersByRole(skip, take, role);
            var count = await _userStore.CountUsersByRole(role);
            var usersList = new UsersList
            {
                Users = users,
                MaxPage = (count % take) == 0 ? count / take : count / take + 1,
                Page = page,
            };
            return usersList;
        }

        private CommentsList GetCommentsList(Guid id, int page = 1)
        {
            int take = 10;
            int skip = (page - 1) * take;
            var comments = _commentService.GetComments(id, take, skip);
            var commentsList = new CommentsList();
            commentsList.Comments = comments.Map<List<Comment>, List<CommentDisplay>>();
            commentsList.ImageId = id;
            var countComments = _commentService.CountComments(id);
            commentsList.MaxPage = (countComments % take) == 0 ? countComments / take : countComments / take + 1;
            commentsList.Page = page;
            return commentsList;
        }

        private string GetPath(string name, string extension)
        {
            StringBuilder builder = new StringBuilder("..\\Files\\");
            builder.AppendFormat("{0}", name);
            return builder.AppendFormat("{0}", extension).ToString();
        }

        private void SendMessage(string selector)
        {
            var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<RatingHub>();
            context.Clients.All.updateNew(selector);
        }
    }
}