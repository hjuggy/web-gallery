﻿using System.Web.Mvc;
using System.Web.Routing;


namespace WebGallery
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(null,
                "",
                new
                {
                    controller = "Image",
                    action = "Index",
                    genre = (string)null,
                    page = 1
                });

            routes.MapRoute(null,
                "Page{page}",
                new { controller = "Image", action = "Index", category = (string)null },
                new { page = @"\d+" }
                );

            routes.MapRoute(
                name: "GenreRoute",
                url: "{controller}/{action}/{genre}/{page}",
                defaults: new { controller = "Image", action = "Index", genre = UrlParameter.Optional, page = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Image", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(null, "{controller}/{action}");
        }
    }
}
